﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procon.CommonLibrary;
using System.Diagnostics;
using System.Windows.Threading;

namespace ProconClient
{
    class Solver
    {
        public Solver()
        {
        }

        public OperationSequence Solve(ProblemSolveData data, Dispatcher dispatcher)
        {
            try
            {
                if (data.ConvertedBoard == null || data.State == ProblemSolveState.Loaded)
                    throw new ArgumentException();
                var pinfo = new ProcessStartInfo()
                {
                    WorkingDirectory = System.IO.Directory.GetCurrentDirectory(),
                    FileName = "solver/solver.exe",
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    //RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                };
                var process = Process.Start(pinfo);
                var problem = data.Problem;
                var board = data.ConvertedBoard.Clone() as int[,];
                int[] map = new int[problem.Width * problem.Height];
                for (int x = 0; x < problem.Width; x++)
                {
                    for (int y = 0; y < problem.Height; y++)
                    {
                        var p = board[x, y];
                        var i = x + y * problem.Width;
                        map[board[x, y]] = i;
                    }
                }
                for (int x = 0; x < problem.Width; x++)
                {
                    for (int y = 0; y < problem.Height; y++)
                    {
                        board[x, y] = map[x + y * problem.Width];
                    }
                }
                Console.WriteLine(problem.SelectCost);
                Console.WriteLine(problem.SwapCost);
                Console.WriteLine(problem.MaxSelect);
                Console.WriteLine(problem.Height);
                Console.WriteLine(problem.Width);
                for (int y = 0; y < problem.Height; y++)
                {
                    for (int x = 0; x < problem.Width; x++)
                    {
                        Console.WriteLine(board[x, y]);
                    }
                }
                process.StandardInput.WriteLine(problem.SelectCost);
                process.StandardInput.WriteLine(problem.SwapCost);
                process.StandardInput.WriteLine(problem.MaxSelect);
                process.StandardInput.WriteLine(problem.Height);
                process.StandardInput.WriteLine(problem.Width);
                for (int y = 0; y < problem.Height; y++)
                {
                    for (int x = 0; x < problem.Width; x++)
                    {
                        process.StandardInput.WriteLine(board[x, y]);
                    }
                }
                //実行時間制限
                process.StandardInput.WriteLine(20 * 1000);
                bool end = false;
                OperationSequence op = null;
                while (!end)
                {
                    var builder = new StringBuilder();
                    while (true)
                    {
                        var str = process.StandardOutput.ReadLine();
                        if (str != "__EOF__" && str != "__EOS__" && str != "")
                        {
                            builder.AppendLine(str);
                        }
                        else
                        {
                            if (str == "__EOF__")
                                end = true;
                            break;
                        }
                    }
                    var hoge = builder.ToString();
                    dispatcher.BeginInvoke(
                        (Action)(() =>
                        {
                            var task = TaskManager.Instance.RequestSubmit(data, hoge);
                            task.Start();
                        }));
                    try
                    {
                        op = OperationSequence.Parse(data.Problem, builder.ToString());
                        data.AddOperation(op);
                    }
                    catch (Exception e)
                    {
                    }
                }

                return op;
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString(), "Error");
                return null;
            }
        }

        public async Task<OperationSequence> SolveAsync(ProblemSolveData data, Dispatcher d)
        {
            return await Task.Run(() => Solve(data, d));
        }
    }
}
