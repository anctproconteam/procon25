﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Linq;
using Procon.CommonLibrary;

namespace ProconClient
{
    public class ClientLib
    {
        private static readonly string PlayerIDFieldName = "playerid";
        private static readonly string AnswerFieldName = "answer";
        private static readonly string ProblemIdFieldName = "problemid";
        private static readonly string SubmitAnswerLocation = "/SubmitAnswer";
        private static readonly string ProblemLocation = "/problem/";
        private static readonly string ProblemFileNameFormat = "prob{0:D2}.ppm";
        //public static string ServerUrl = "172.16.1.2";
        public static string ServerUrl = "localhost";

        public static string SubmitAnswer(string serverURL, int problemId, string playerid, string ans)
        {
            byte[] res;
            try
            {
                using (var wc = new WebClient())
                {
                    var nvc = new NameValueCollection();
                    nvc.Add(PlayerIDFieldName, playerid);
                    nvc.Add(ProblemIdFieldName, string.Format("{0:D2}", problemId));
                    var ansstr = ans.ToString();
                    ansstr = ansstr.Select<char, char>(
                        (c) =>
                        {
                            switch (c)
                            {
                                case 'U':
                                    c = 'D';
                                    break;
                                case 'D':
                                    c = 'U';
                                    break;
                            }
                            return c;
                        }
                        ).GenerateString();

                    nvc.Add(AnswerFieldName, ans.ToString());
                    res = wc.UploadValues("http://" + serverURL + SubmitAnswerLocation, nvc);
                }
                return Encoding.UTF8.GetString(res);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString());
                return "";
            }
        }

        public static byte[] GetProblem(string serverURL, int id)
        {
            var problemId = string.Format("{0:D2}", id);
            using (var wc = new WebClient())
            {
                return wc.DownloadData("http://" + serverURL + ProblemLocation + GetProblemFileName(problemId));
            }
        }

        public static string GetProblemFileName(string problemId)
        {
            int numProblemId = int.Parse(problemId);
            return string.Format(ProblemFileNameFormat, numProblemId);
        }
    }
}