﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Procon.CommonLibrary;

namespace ProconClient
{
    /// <summary>
    /// ProblemDetail.xaml の相互作用ロジック
    /// </summary>
    public partial class ProblemDetail : UserControl, IDisposable
    {
        ProblemSolveData data;
        public ProblemDetail()
        {
            InitializeComponent();
        }
        public ProblemDetail(ProblemSolveData solveData)
        {
            data = solveData;
            DataContext = data;
            InitializeComponent();
            var img = new Image();
            img.Source = data.Problem.Image.ToBitmap();
            OriginalImageTabItem.Content = img;
            UpdateConvertedImage();
            if (data.ConvertedBoard != null)
                TabControl.TabIndex = 1;
            data.PropertyChanged += UpdateData;
        }

        private async void ImageAnalyze(object sender, RoutedEventArgs e)
        {
            var task = TaskManager.Instance.RequestImageAnalyze(data);
            task.Succeed += (_) =>
            {
                this.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        UpdateConvertedImage();
                    })
                    );
            };
            task.Start();
        }

        void UpdateConvertedImage()
        {
            if (data.ConvertedBoard == null)
                return;
            ConvertedImage.Children.Clear();
            ConvertedImage.ColumnDefinitions.Clear();
            ConvertedImage.RowDefinitions.Clear();
            ConvertedImage.Background = Brushes.Yellow;
            for (int i = 0; i < data.Problem.Width; i++)
                ConvertedImage.ColumnDefinitions.Add(new ColumnDefinition());
            for (int i = 0; i < data.Problem.Height; i++)
                ConvertedImage.RowDefinitions.Add(new RowDefinition());
            for (int x = 0; x < data.Problem.Width; x++)
            {
                for (int y = 0; y < data.Problem.Height; y++)
                {
                    var image = new Image();
                    image.Source = data.Problem.SplitedImage[data.ConvertedBoard[x, y]].ToBitmap();
                    Grid.SetColumn(image, x);
                    Grid.SetRow(image, y);
                    ConvertedImage.Children.Add(image);
                    int xx = x;
                    int yy = y;
                    image.MouseDown += (obj, e) =>
                    {
                        ImagePartClick(xx, yy);
                    };
                    if (SwapRadioButton.IsChecked.Value)
                    {
                        if (selectedCell[0] != null && selectedCell[0].Item1 == x && selectedCell[0].Item2 == y)
                        {
                            image.Opacity = 0.5;
                        }
                    }
                    else
                    {
                        if (selectedCell[0] != null && selectedCell[1] != null)
                        {
                            if (x.IsInnerOf(selectedCell[0].Item1, selectedCell[1].Item1) && y.IsInnerOf(selectedCell[0].Item2, selectedCell[1].Item2))
                            {
                                image.Opacity = 0.5;
                            }
                        }
                        else if (selectedCell[0] != null && selectedCell[0].Item1 == x && selectedCell[0].Item2 == y)
                        {
                            image.Opacity = 0.8;
                        }
                    }
                }
            }
            const int size = 60;
            //ConvertedImage.Width = size * data.Problem.Width;
            //ConvertedImage.Height = size * data.Problem.Height;
        }

        Tuple<int, int>[] selectedCell = new Tuple<int,int>[2];

        void ImagePartClick(int x, int y)
        {
            if (SwapRadioButton.IsChecked.Value)
            {
                if (selectedCell[0] == null)
                {
                    selectedCell[0] = Tuple.Create(x, y);
                }
                else
                {
                    var s1 = selectedCell[0];
                    var s2 = Tuple.Create(x, y);
                    var tmp = data.ConvertedBoard[s1.Item1, s1.Item2];
                    data.ConvertedBoard[s1.Item1, s1.Item2] = data.ConvertedBoard[s2.Item1, s2.Item2];
                    data.ConvertedBoard[s2.Item1, s2.Item2] = tmp;
                    selectedCell[0] = selectedCell[1] = null;
                }
            }
            else
            {
                if (selectedCell[0] == null)
                {
                    selectedCell[0] = Tuple.Create(x, y);
                }
                else if (selectedCell[1] == null)
                {
                    selectedCell[1] = Tuple.Create(x, y);
                    int minx = Math.Min(selectedCell[0].Item1, selectedCell[1].Item1);
                    int maxx = Math.Max(selectedCell[0].Item1, selectedCell[1].Item1);
                    int miny = Math.Min(selectedCell[0].Item2, selectedCell[1].Item2);
                    int maxy = Math.Max(selectedCell[0].Item2, selectedCell[1].Item2);
                    selectedCell[0] = Tuple.Create(minx, miny);
                    selectedCell[1] = Tuple.Create(maxx, maxy);
                }
                else
                {
                    selectedCell[0] = selectedCell[1] = null;
                }
            }
            UpdateConvertedImage();
        }

        private async void SolveSearchButton_Click(object sender, RoutedEventArgs e)
        {
            var task = TaskManager.Instance.RequestSolveSearch(data, Dispatcher);
            task.Succeed += (_) =>
            {
                this.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        UpdateLayout();
                        UpdateCostData();
                    })
                    );
                //data.Submit();
                //data.State = ProblemSolveState.Submited;
            };
            task.Start();
        }

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            var board = data.ConvertedBoard.Clone() as int[,];
            var problem = data.Problem;
            int[] map = new int[problem.Width * problem.Height];
            for (int x = 0; x < problem.Width; x++)
            {
                for (int y = 0; y < problem.Height; y++)
                {
                    var p = board[x, y];
                    var i = x + y * problem.Width;
                    map[board[x, y]] = i;
                }
            }
            for (int x = 0; x < problem.Width; x++)
            {
                for (int y = 0; y < problem.Height; y++)
                {
                    board[x, y] = map[x + y * problem.Width];
                }
            }
            var previewWindow = new ProconSimulator.MainWindow(board, data.Operations.Last(), data.Problem);
            previewWindow.ShowDialog();
        }

        public void Dispose()
        {
            if (data != null)
            {
                data.PropertyChanged -= UpdateData;
            }
        }

        void UpdateData(object obj, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var name = e.PropertyName;
            if (name == "BestOperation")
            {
                BestCostView.Content = data.BestOperation.Cost;
            }
        }
        void UpdateCostData()
        {
            BestCostView.Content = data.BestOperation.Cost;
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            TaskManager.Instance.RequestSubmit(data);
        }

        void Swap(int x1, int y1, int x2, int y2)
        {
            var tmp = data.ConvertedBoard[x1, y1];
            data.ConvertedBoard[x1, y1] = data.ConvertedBoard[x2, y2];
            data.ConvertedBoard[x2, y2] = tmp;
        }

        private void UpShift_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCell[0] == null || selectedCell[1] == null)
                return;
            int minx = Math.Min(selectedCell[0].Item1, selectedCell[1].Item1);
            int maxx = Math.Max(selectedCell[0].Item1, selectedCell[1].Item1);
            int miny = Math.Min(selectedCell[0].Item2, selectedCell[1].Item2);
            int maxy = Math.Max(selectedCell[0].Item2, selectedCell[1].Item2);
            for (int x = minx; x <= maxx; x++)
            {
                var tmp = data.ConvertedBoard[x, 0];
                for (int y = 0; y < data.Problem.Height - 1; y++)
                {
                    Swap(x, y, x, y + 1);
                }
                data.ConvertedBoard[x, data.Problem.Height - 1] = tmp;
            }
            UpdateConvertedImage();
        }

        private void LeftShift_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCell[0] == null || selectedCell[1] == null)
                return;
            int minx = Math.Min(selectedCell[0].Item1, selectedCell[1].Item1);
            int maxx = Math.Max(selectedCell[0].Item1, selectedCell[1].Item1);
            int miny = Math.Min(selectedCell[0].Item2, selectedCell[1].Item2);
            int maxy = Math.Max(selectedCell[0].Item2, selectedCell[1].Item2);
            for (int y = miny; y <= maxy; y++)
            {
                var tmp = data.ConvertedBoard[0, y];
                for (int x = 0; x < data.Problem.Width - 1; x++)
                {
                    Swap(x, y, x + 1, y);
                }
                data.ConvertedBoard[data.Problem.Width - 1, y] = tmp;
            }
            UpdateConvertedImage();
        }

        private void RightShift_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCell[0] == null || selectedCell[1] == null)
                return;
            int minx = Math.Min(selectedCell[0].Item1, selectedCell[1].Item1);
            int maxx = Math.Max(selectedCell[0].Item1, selectedCell[1].Item1);
            int miny = Math.Min(selectedCell[0].Item2, selectedCell[1].Item2);
            int maxy = Math.Max(selectedCell[0].Item2, selectedCell[1].Item2);
            for (int y = miny; y <= maxy; y++)
            {
                var tmp = data.ConvertedBoard[data.Problem.Width - 1, y];
                for (int x = data.Problem.Width - 1; x > 0; x--)
                {
                    Swap(x, y, x - 1, y);
                }
                data.ConvertedBoard[0, y] = tmp;
            }
            UpdateConvertedImage();
        }

        private void DownShift_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCell[0] == null || selectedCell[1] == null)
                return;
            int minx = Math.Min(selectedCell[0].Item1, selectedCell[1].Item1);
            int maxx = Math.Max(selectedCell[0].Item1, selectedCell[1].Item1);
            int miny = Math.Min(selectedCell[0].Item2, selectedCell[1].Item2);
            int maxy = Math.Max(selectedCell[0].Item2, selectedCell[1].Item2);
            for (int x = minx; x <= maxx; x++)
            {
                var tmp = data.ConvertedBoard[x, data.Problem.Height - 1];
                for (int y = data.Problem.Height - 1; y > 0; y--)
                {
                    Swap(x, y, x, y - 1);
                }
                data.ConvertedBoard[x, 0] = tmp;
            }
            UpdateConvertedImage();

        }
    }
}
