﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ProconClient
{
    public enum TaskState { Created, Waiting, Started, Finished, Failed }
    public abstract class TaskListItem : INotifyPropertyChanged
    {
        public int No { get; set; }
        abstract public string Kind { get; }
        /// <summary>
        /// 進捗率。100が最大。
        /// </summary>
        int progress;
        public int Progress
        {
            get { return progress; }
            set { progress = value; OnPropertyChanged("Progress"); }
        }

        TaskState state = TaskState.Waiting;
        public TaskState State
        {
            get { return state; }
            set { state = value; OnPropertyChanged("State"); }
        }
        public ProblemSolveData Data { get; protected set; }

        public TaskListItem NeedTask = null;

        public bool CanStart
        {
            get
            {
                return State == TaskState.Waiting && (NeedTask == null || NeedTask.State == TaskState.Finished);
            }
        }

        string message = "";
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }

        public void Start()
        {
            if (State == TaskState.Waiting)
                State = TaskState.Waiting;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        abstract public Task Run();

        public event Action<TaskListItem> Succeed;

        protected void OnSucceed()
        {
            if (Succeed != null)
                Succeed(this);
        }
    }

    public class DownloadTask : TaskListItem
    {
        public override string Kind
        {
            get { return "Download"; }
        }
        public readonly int ProblemNo;
        public ProblemSolveData SolveData { get; private set; }

        public DownloadTask(int no)
        {
            ProblemNo = no;
        }

        public override async Task Run()
        {
            State = TaskState.Started;
            try
            {
                var data = await Task.Run(() => ClientLib.GetProblem(ClientLib.ServerUrl, ProblemNo));
                var ppm = Procon.PPMLoader.ImageInstance.Load(new System.IO.MemoryStream(data));
                var pconst = new Procon.CommonLibrary.ProblemConstructor(ppm.Comment)
                {
                    No = ProblemNo,
                    image = ppm,
                };
                pconst.splitedImage = ppm.SplitImage(pconst.Width, pconst.Height);
                var problem = pconst.Construct();
                SolveData = new ProblemSolveData(problem);
                State = TaskState.Finished;
                OnSucceed();
            }
            catch (Exception e)
            {
                State = TaskState.Failed;
            }
        }
    }

    public class SolverTask : TaskListItem
    {
        public override string Kind { get { return "Solve_Serch"; } }
        Dispatcher dispatcher;

        public SolverTask(ProblemSolveData data, Dispatcher dispatcher)
        {
            this.Data = data;
            this.dispatcher = dispatcher;
        }

        public override async Task Run()
        {
            if (Data.ConvertedBoard == null)
                throw new ArgumentException();
            State = TaskState.Started;
            var result = await new Solver().SolveAsync(Data, dispatcher);
            if (result != null)
            {
                State = TaskState.Finished;
                Progress = 100;
                Data.AddOperation(result);
                Data.State = ProblemSolveState.Solved;
                OnSucceed();
            }
            else
            {
                State = TaskState.Failed;
            }
        }
    }
    public class ImageAnalyzeTask : TaskListItem
    {
        public override string Kind { get { return "ImageAnalyze"; } }

        public ImageAnalyzeTask(ProblemSolveData data)
        {
            this.Data = data;
        }

        public override async Task Run()
        {
            State = TaskState.Started;
            try
            {
                var result = await new ImageAnalysis.RestoredImage().GetBoardAsync(Data.Problem);
                if (result != null)
                {
                    Data.ConvertedBoard = result.Clone() as int[,];
                    Data.State = ProblemSolveState.Converted;
                    Progress = 100;
                    State = TaskState.Finished;
                    OnSucceed();
                }
                else
                {
                    State = TaskState.Failed;
                }
            }
            catch (Exception e)
            {
            }
        }
    }
    public class SubmitTask : TaskListItem
    {
        public override string Kind { get { return "Submit"; } }
        string answer = null;

        public SubmitTask(ProblemSolveData data)
        {
            this.Data = data;
        }
        public SubmitTask(ProblemSolveData data, string answer)
        {
            this.Data = data;
            this.answer = answer;
        }

        public override async Task Run()
        {
            State = TaskState.Started;
            try
            {
                var result = await Task.Run(() => Data.Submit());
                Message = result;
                State = TaskState.Finished;
                Data.State = ProblemSolveState.Submited;
                OnSucceed();
            }
            catch (Exception e)
            {
                State = TaskState.Failed;
            }
        }
    }
}
