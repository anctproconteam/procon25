﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ProconClient
{
    public class TaskManager : INotifyPropertyChanged
    {
        private TaskManager() { }
        int taskId = 0;

        object lockObject = new object();

        public readonly int MaxRunTask = 4;

        ObservableCollection<TaskListItem> taskList = new ObservableCollection<TaskListItem>();

        public ObservableCollection<TaskListItem> TaskList { get { return taskList; } }

        public void AddTask(TaskListItem task)
        {
            lock (lockObject)
            {
                taskList.Add(task);
            }
            OnPropertyChanged("TaskList");
        }

        public void Start()
        {
            Task.Run(() => Loop());
        }

        void Loop()
        {
            while (true)
            {
                Thread.Sleep(50);
                lock (lockObject)
                {
                    while (TaskList.Any(t => t.CanStart) && TaskList.Count(t => t.State == TaskState.Started) < MaxRunTask)
                    {
                        var nextTask = TaskList.First(t => t.CanStart);
                        nextTask.Run();
                    }
                }
            }
        }

        public ImageAnalyzeTask RequestImageAnalyze(ProblemSolveData data)
        {
            var task = new ImageAnalyzeTask(data)
            {
                No = taskId++,
            };
            AddTask(task);
            return task;
        }

        public SolverTask RequestSolveSearch(ProblemSolveData data, Dispatcher d)
        {
            var task = new SolverTask(data, d)
            {
                No = taskId++,
            };
            AddTask(task);
            return task;
        }

        public DownloadTask RequestDownloadImage(int no)
        {
            var task = new DownloadTask(no)
            {
                No = taskId++
            };
            AddTask(task);
            return task;
        }

        public SubmitTask RequestSubmit(ProblemSolveData data)
        {
            var task = new SubmitTask(data)
            {
                No = taskId++
            };
            AddTask(task);
            return task;
        }
        public SubmitTask RequestSubmit(ProblemSolveData data, string answer)
        {
            var task = new SubmitTask(data, answer)
            {
                No = taskId++
            };
            AddTask(task);
            return task;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        static private TaskManager instance;
        static public TaskManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new TaskManager();
                return instance;
            }
        }
    }
}
