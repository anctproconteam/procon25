﻿using Procon.CommonLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProconClient
{
    public enum ProblemSolveState
    {
        Loaded, Converted, Solved, Submited
    }
    public class ProblemSolveData : INotifyPropertyChanged
    {
        public object lockObject = new object();
        public Problem Problem { get; private set; }
        ProblemSolveState state;
        public ProblemSolveState State
        {
            get { return state; }
            set
            {
                state = value;
                OnPropertyChanged("State");
                OnPropertyChanged("IsConverted");
                OnPropertyChanged("IsSolved");
            }
        }
        List<OperationSequence> operations = new List<OperationSequence>();
        public IReadOnlyList<OperationSequence> Operations
        {
            get { return operations; }
        }
        int[,] convertedBoard;
        public int[,] ConvertedBoard
        {
            get { return convertedBoard; }
            set
            {
                lock (lockObject)
                {
                    convertedBoard = value;
                    OnPropertyChanged("ConvertedBoard");
                }
            }
        }

        public void AddOperation(OperationSequence seaquence)
        {
            var b1 = BestOperation;
            operations.Add(seaquence);
            var b2 = BestOperation;
            //OnPropertyChanged("Operations");
            //if (b1 != b2)
            //    OnPropertyChanged("BestOperation");
        }

        public OperationSequence BestOperation
        {
            get
            {
                return operations.LastOrDefault();
                if (!Operations.Any())
                    return null;
                return Operations.OrderBy(op => op.Cost).First();
            }
        }
            

        public bool IsConverted
        {
            get { return convertedBoard != null; }
        }
        public bool IsSolved
        {
            get { return Operations.Any(); }
        }

        public ProblemSolveData(Problem problem)
        {
            Problem = problem;
        }

        public string Submit()
        {
            return ClientLib.SubmitAnswer(ClientLib.ServerUrl, Problem.No, "1600612906", BestOperation.ToString());
        }
        public string Submit(string answer)
        {
            return ClientLib.SubmitAnswer(ClientLib.ServerUrl, Problem.No, "1600612906", answer);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
