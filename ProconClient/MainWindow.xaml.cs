﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProconClient
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        List<ProblemSolveData> data = new List<ProblemSolveData>();

        public MainWindow()
        {
            InitializeComponent();
            ServerAddressTextBox.Text = ClientLib.ServerUrl;
            ServerAddressTextBox.IsEnabled = false;
            TaskManager.Instance.Start();
            //var task = Task.Run(() => new ImageAnalysis.RestoredImage().GetBoard(@"C:\Users\procon\Documents\procon25\ImageAnalysis\bin\Debug\images\prob03.ppm"));
            //task.Wait();
            //var result = task.Result; 
        }

        private void LoadFromFile(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.FilterIndex = 1;
            openFileDialog.Filter = "all|*.*";
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog().Value)
            {
                foreach (var filename in openFileDialog.FileNames)
                {
                    var ppm = Procon.PPMLoader.ImageInstance.LoadByFile(filename);
                    var pconst = new Procon.CommonLibrary.ProblemConstructor(ppm.Comment)
                    {
                        No = data.Count(),
                        ImagePath = openFileDialog.FileName,
                        image = ppm,
                    };
                    pconst.splitedImage = ppm.SplitImage(pconst.Width, pconst.Height);
                    var problem = pconst.Construct();
                    var pdata = new ProblemSolveData(problem);
                    data.Add(pdata);
                    var li = new ProblemListItem(pdata);
                    li.Click += (se, ea) =>
                    {
                        MainPanel.Children.Clear();
                        MainPanel.Children.Add(
                            new ProblemDetail(pdata)
                            );
                    };
                    ProblemsStackPanel.Children.Add(li);
                }
            }
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            var max = int.Parse(MaxQuestionNo.Text);
            int i = max;
            var task = TaskManager.Instance.RequestDownloadImage(i);
            task.Succeed += (_) =>
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    try
                    {
                        var pdata = task.SolveData;
                        data.Add(pdata);
                        var li = new ProblemListItem(pdata);
                        li.Click += (se, ea) =>
                        {
                            MainPanel.Children.Clear();
                            MainPanel.Children.Add(
                                new ProblemDetail(pdata)
                                );
                        };
                        ProblemsStackPanel.Children.Add(li);
                        var problem = task.SolveData.Problem;
                        var itask = TaskManager.Instance.RequestImageAnalyze(task.SolveData);
                        var stask = TaskManager.Instance.RequestSolveSearch(task.SolveData, this.Dispatcher);
                        //var dtask = TaskManager.Instance.RequestSubmit(task.SolveData);
                        //dtask.NeedTask = stask;
                        stask.NeedTask = itask;
                        stask.Start();
                        itask.Start();
                        //dtask.Start();
                    }
                    catch (Exception ee)
                    {
                    }
                }));
            };
            task.Start();
        }
    }
}
