﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Procon.CommonLibrary;
using System.ComponentModel;

namespace ProconClient
{
    /// <summary>
    /// ProblemListItem.xaml の相互作用ロジック
    /// </summary>
    public partial class ProblemListItem : UserControl
    {
        public readonly ProblemSolveData data;

        [Obsolete]
        public ProblemListItem()
        {
            InitializeComponent();

        }
        public ProblemListItem(ProblemSolveData data)
        {
            this.data = data;
            DataContext = data;
            InitializeComponent();
            Initialize();
        }

        void Initialize()
        {
            //DetailView.Children.Clear();
            var Problem = data.Problem;
            //DetailView.Children.Add(new Label() { Content = String.Format("盤面サイズ : {0}x{1}", Problem.Width, Problem.Height) });
            //DetailView.Children.Add(new Label() { Content = String.Format("交換コスト : {0}", Problem.SwapCost) });
            //DetailView.Children.Add(new Label() { Content = String.Format("選択コスト : {0}", Problem.SelectCost) });
            //DetailView.Children.Add(new Label() { Content = String.Format("選択回数上限 : {0}", Problem.MaxSelect) });
            //DetailView.Children.Add(new Label() { Content = String.Format("状態 : {0}", "--") });
            PrevImage.Source = Problem.Image.ToBitmap();
            var b = new Button();
        }

        public event RoutedEventHandler Click;

        private void OnClick(object sender, RoutedEventArgs e)
        {
            if(Click != null)
                Click(sender, e);
        }

    }
    class ProblemListItemColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Brush brush = Brushes.White;
            var state = (ProblemSolveState)value;
            switch (state)
            {
                case ProblemSolveState.Loaded:
                    return Brushes.White;
                case ProblemSolveState.Converted:
                    return Brushes.Yellow;
                case ProblemSolveState.Solved:
                    return Brushes.LightGreen;
                case ProblemSolveState.Submited:
                    return Brushes.LightBlue;
            }
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
