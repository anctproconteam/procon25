#pragma once

#include <algorithm>
#include <vector>
#include <cmath>
#include <random>
#include <ctime>
#include "Settings.hpp"
#include "Answer.hpp"
#include "Simulater.hpp"
using namespace std;

class Answer;

class Solver
{
private:
	Board board;
	int max_select;
	int select_cost;
	int swap_cost;

	int WIDTH;
	int HEIGHT;
	
public:
	Solver(Board, int max_select, int select_cost, int swap_cost);
	Answer Solve(int time_limit);
	vector<Answer> Swaps(const Answer& answer) const;
	vector<Answer> Selects(const Answer& answer, const double P2) const;
	vector<Answer> Elites(vector<Answer>& answers, const int BEAM_WIDTH, const double LUCKY_RATIO, const bool dame);
	int Similarity(const Answer& b1, const Answer& b2);
};
