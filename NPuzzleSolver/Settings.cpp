#include "Settings.hpp"
#include <random>

void WriteBoard(const Board& board)
{
	const int width = (int) board[0].size();
	const int height = (int) board.size();

	for(int y = 0; y < height; y++) {
		for(int x = 0; x < width; x++) {
			std::cerr << board[y][x] << (x + 1 == width ? '\n' : ' ');
		}
	}
}


float frand()
{
	static std::random_device seed_gen;
	static std::mt19937 engine(seed_gen());
	static std::uniform_real_distribution<> dist(0.0, 1.0);
	return dist(engine);
}

int htoi(char a)
{
	return ('0' <= a && a<= '9') ? (a - '0') : (10 + a - 'A');
}

std::string itoh(int i)
{
	return std::string() + (char)(i < 10 ? '0' + i : 'A' + i - 10);
}

std::string vtos(const std::vector<std::string>& v)
{
	std::string result = "";
	for(int i = 0; i < v.size(); i++) {
		result += v[i] + (i + 1 == v.size() ? "" : "\n");
	}
	
	return result;
}

template <typename T>
std::vector<T> operator+(const std::vector<T> &A, const std::vector<T> &B)
{
	std::vector<T> AB;
	AB.reserve( A.size() + B.size() );                // preallocate memory
	AB.insert( AB.end(), A.begin(), A.end() );        // add A;
	AB.insert( AB.end(), B.begin(), B.end() );        // add B;
	return AB;
}

template <typename T>
std::vector<T> &operator+=(std::vector<T> &A, const std::vector<T> &B)
{
	A.reserve( A.size() + B.size() );                // preallocate memory without erase original data
	A.insert( A.end(), B.begin(), B.end() );         // add B;
	return A;                                        // here A could be named AB
}
