#include "Answer.hpp"

Answer::Answer(const Board& board, int select_cost, int swap_cost, int x, int y)
	: select_cost(select_cost), swap_cost(swap_cost), last_direction(-1), cost(select_cost), select_count(1), last_is_select(true), x(x), y(y), lock_bottom(-1), lock_up((int)board.size()), lock_left(-1), lock_right((int)board[0].size()), board(board), evaluation_basis(CalcEvaluationBasis()), solvable(Solvable())
{
	all.push_back(itoh(x) + itoh(y));
}

Answer Answer::Swap(int direction) const
{
	auto ret = *this;
	
	int nx = ret.x + DX[direction];
	int ny = ret.y + DY[direction];
	
	swap(ret.board[ret.y][ret.x], ret.board[ny][nx]);
	ret.Lock(ret.x, ret.y, nx, ny);

	ret.x = nx;
	ret.y = ny;

	ret.cost += ret.swap_cost;
	ret.tail += DIRECTIONS[direction];

	ret.last_is_select = false;
	ret.last_direction = direction;
	
	const int width = (int) board[0].size();
	const int ttx = board[y][x] % width;
	const int tty = board[y][x] / width;
	const int tx = board[ny][nx] % width;
	const int ty = board[ny][nx] / width;
	
	ret.evaluation_basis -= (abs(nx - tx) + abs(ny - ty)) * (abs(ttx - tx) + abs(tty - ty));
	ret.evaluation_basis += (abs(x - tx) + abs(y - ty)) * (abs(ttx - tx) + abs(tty - ty));

	return ret;
}

bool Answer::Stupid(int direction) const
{
	if(direction == 0 && last_direction == 1)
		return true;
	if(direction == 1 && last_direction == 0)
		return true;
	if(direction == 2 && last_direction == 3)
		return true;
	if(direction == 3 && last_direction == 2)
		return true;
	
	return false;
}

Answer Answer::Select(int x, int y) const
{
	assert(!last_is_select);

	auto ret = *this;
	ret.Lock(this->x, this->y, x, y);
	
	ret.x = x;
	ret.y = y;

	if(ret.tail.size() != 0) {
		ret.all.push_back(to_string(ret.tail.size()));
		ret.all.push_back(ret.tail);
		ret.tail = "";
	}

	ret.all.push_back(itoh(x) + itoh(y));
	
	ret.cost += ret.select_cost;
	ret.select_count++;
	
	ret.last_is_select = true;
	ret.last_direction = -1;
	
	ret.evaluation_basis = ret.CalcEvaluationBasis();
	ret.solvable = ret.Solvable();
	
	return ret;
}

vector<string> Answer::Procedure() const
{
	vector<string> tmp = vector<string>(1, to_string(select_count - last_is_select));
	tmp.insert(tmp.end(), all.begin(), all.end() - (int)last_is_select);

	if(tail.size() != 0) {
		tmp.push_back(to_string(tail.size()));
		tmp.push_back(tail);
	}

	return tmp;
}

void Answer::Lock(int ox, int oy, int nx, int ny)
{
	if(oy != ny) {
		if(lock_bottom + 1 == oy && oy + 2 < lock_up && Correct_row(oy)) {
			lock_bottom++;
		}
		
		if(lock_up - 1 == oy && oy - 2 > lock_bottom && Correct_row(oy)) {
			lock_up--;
		}
	}

	if(ox != nx) {
		if(lock_left + 1 == ox && ox + 2 < lock_right && Correct_column(ox)) {
			lock_left++;
		}
		
		if(lock_right - 1 == ox && ox - 2 > lock_left && Correct_column(ox)) {
			lock_right--;
		}
	}
}

bool Answer::Correct_row(int row) const
{
	const int width = (int) board[0].size();
	
	for(int i = 0; i < width; i++) {
		if(board[row][i] != row * width + i) {
			return false;
		}
	}
	
	return true;
}

bool Answer::Correct_column(int column) const
{
	const int width = (int) board[0].size();
	const int height = (int) board.size();
	
	for(int i = 0; i < height; i++) {
		if(board[i][column] != i * width + column) {
			return false;
		}
	}
	
	return true;
}

int Answer::CompareTo(const Answer& other) const
{
	int sv = this->Evaluate ();
	int ov = other.Evaluate();

	if(sv != ov)
		return sv > ov ? 1 : -1;

	if(this-> cost != other.cost)
		return this->cost > other.cost ? 1 : -1;

	return 0;
}

bool Answer::operator < (const Answer& other) const
{
	return this->CompareTo(other) < 0;
}

bool Answer::operator > (const Answer& other) const
{
	return this->CompareTo(other) > 0;
}

int Answer::ValidSquare() const
{
	return (lock_up - lock_bottom - 1) * (lock_right - lock_left - 1);
}

bool Answer::Solvable() const
{
	const int WIDTH = (int) board[0].size();
	const int width = lock_right - lock_left - 1;
	const int height = lock_up - lock_bottom - 1;
	
	Board c(height, vector<int>(width));

	for(int _y = lock_bottom + 1; _y < lock_up; _y++) {
		for(int _x = lock_left + 1; _x < lock_right; _x++) {
			int ty = board[_y][_x] / WIDTH;
			int bv = WIDTH * (lock_bottom + 1) + lock_left + 1;
			
			c[_y - lock_bottom - 1][_x - lock_left - 1] = board[_y][_x] - bv - (WIDTH - width) * (ty - lock_bottom - 1);
		}
	}
	
	return Solvable(c, x - lock_left - 1, y - lock_bottom - 1);
}

bool Answer::Solvable(int max_select) const
{
	if(select_count < max_select)
		return true;

	return solvable;
}

bool Answer::Solvable(const Board& b, int ax, int ay) const
{
	const int width = (int) b[0].size();
	const int height = (int) b.size();

	vector<int> key;
	for(int _y = 0; _y < height; _y++) {
		for(int _x = 0; _x < width; _x++) {
			int index = _y * width + (_y % 2 == 0 ? _x : width - _x - 1);
			
			if(b[ay][ax] == index)
				continue;
			
			key.push_back(index);
		}
	}
	
	vector<int> value;
	for(int _y = 0; _y < height; _y++) {
		for(int _x = 0; _x < width; _x++) {
			int xx = (_y % 2 == 0 ? _x : width - _x - 1);
			if(_y == ay && xx == ax)
				continue;
			
			value.push_back(b[_y][xx]);
		}
	}
	
	vector<int> subs(width * height);
	subs[b[ay][ax]] = b[ay][ax];
	
	for(int i = 0; i < key.size(); i++) {
		subs[key[i]] = value[i];
	}
	
	vector<int> flag(width * height, true);
	int v = 0;
	for(int i = 0; i < width * height; i++) {
		if(flag[i]) {
			flag[i] = false;
			v += Substitution(subs, flag, subs[i]);
		}
	}

	return v % 2 == 0;
}

int Answer::Substitution(const vector<int>& subs, vector<int>& flag, int i) const
{
	if(!flag[i])
		return 0;
	
	flag[i] = false;
	return 1 + Substitution(subs, flag, subs[i]);
}

int Answer::CalcEvaluationBasis()
{
	const int width = (int) board[0].size();
	const int height = (int) board.size();
	const int ttx = board[y][x] % width;
	const int tty = board[y][x] / width;
	
	int value = 0;
	
	for(int _x = 0; _x < width; _x++) {
		for(int _y = 0; _y < height; _y++) {
			if(x == _x && y == _y)
				continue;
			
			const int tx = board[_y][_x] % width;
			const int ty = board[_y][_x] / width;
			
			value += (abs(_x - tx) + abs(_y - ty)) * (abs(ttx - tx) + abs(tty - ty));
		}
	}
	
	return value;
}

int Answer::Evaluate() const
{
	return evaluation_basis * (lock_right - lock_left - 1) * (lock_up - lock_bottom - 1);
}
