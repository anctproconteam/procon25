#include "Simulater.hpp"

Simulater::Simulater(Board board) : board(board)
{
	for(int i = 0; i < 4; i++) {
		dist[(int)DIRECTIONS[i]] = i;
	}
}

Board Simulater::Simulate(const vector<string>& procedure) const
{	
	auto b = board;

	int n = stoi(procedure[0]);
	for(int i = 0; i < n; i++) {
		int x = htoi(procedure[3 * i + 1][0]);
		int y = htoi(procedure[3 * i + 1][1]);

		// the last sequence is select proc
		if(3 * i + 2 == procedure.size())
			break;

		int m = stoi(procedure[3 * i + 2]);
		string s = procedure[3 * i + 3];

		for(int j = 0; j < m; j++) {
			int nx = x + DX[dist[(int)s[j]]];
			int ny = y + DY[dist[(int)s[j]]];

			swap(b[y][x], b[ny][nx]);

			x = nx;
			y = ny;
		}
	}

	return b;
}

