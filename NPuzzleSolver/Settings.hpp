#pragma once

#include <iostream>
#include <vector>
#include <sstream>
#include <cassert>
#include <string>
typedef std::vector<std::vector<int>> Board;

const int DX[] = { 0, 0, 1, -1 };
const int DY[] = { 1, -1, 0, 0 };
const char DIRECTIONS[] = { 'D', 'U', 'R', 'L' };

void WriteBoard(const Board& board);
float frand();
int htoi(char a);
std::string itoh(int i);
std::string vtos(const std::vector<std::string>& v);
template <typename T>
std::vector<T> operator+(const std::vector<T> &A, const std::vector<T> &B);
template <typename T>
std::vector<T> &operator+=(std::vector<T> &A, const std::vector<T> &B);