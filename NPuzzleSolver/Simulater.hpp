#pragma once

#include <map>
#include <vector>
#include "Settings.hpp"
using namespace std;

class Simulater
{
private:
	int dist['U'+1];
	Board board;

public:
	Simulater(Board board);
	Board Simulate(const vector<string>& procedure) const;
};
