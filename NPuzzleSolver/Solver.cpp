#include "Solver.hpp"
#include <chrono>

Solver::Solver(Board board, int max_select, int select_cost, int swap_cost)
	: board(board), max_select(max_select), select_cost(select_cost), swap_cost(swap_cost),
	  WIDTH((int)board[0].size()), HEIGHT((int)board.size())
{ }

Answer Solver::Solve(int time_limit)
{
	auto begin_time = std::chrono::system_clock::now();
	
	int BEAM_WIDTH = 50;
	const int DEPTH = 3;
	double P1 = 0.01;
	double P2 = 0.01;
	double LUCKY_RATIO = 0.2;

	vector<Answer> now;

	for(int x = 0; x < WIDTH; x++) {
		for(int y = 0; y < HEIGHT; y++) {
			now.push_back(Answer(board, select_cost, swap_cost, x, y));
		}
	}
	
	Answer best = now.front();
	int prev_evalue = best.Evaluate();
	int no_update_times = 0;

	for(int i = 0; best.Evaluate() != 0; i++) {
		auto elapsed_time = chrono::system_clock::now() - begin_time;
		if(chrono::duration_cast<chrono::milliseconds>(elapsed_time).count() > time_limit) {
			cout << vtos(best.Procedure()) << endl;
			cout << "__EOS__" << endl;
			begin_time = chrono::system_clock::now();
		}
		
		vector<Answer> nexts(1, best);
		
		for(int j = 0; j < now.size(); j++) {
			vector<Answer> node_now(1, now[j]);
			
			int to = DEPTH;
			int ddepth = 3;
			
			if(j < BEAM_WIDTH * 0.1) to += ddepth;
			else if(frand() < 0.1) to += ddepth;
			
			for(int k = 0; k < to; k++) {
				vector<Answer> node_nexts;
				
				for(auto n : node_now) {
					auto swaps = Swaps(n);
					node_nexts.insert(node_nexts.end(), swaps.begin(), swaps.end());
					
					if(frand() > P1)
						continue;
					
					auto selects = Selects(n, P2);
					node_nexts.insert(node_nexts.end(), selects.begin(), selects.end());
				}

				auto pred = [this](const Answer& a) { return !a.Solvable(max_select); };
				auto it = remove_if(node_nexts.begin(), node_nexts.end(), pred);
				node_nexts.erase(it, node_nexts.end());
				node_now = node_nexts;
				
				if(!node_now.empty()) {
					best = min(best, *min_element(node_now.begin(), node_now.end()));
				}
			}
			
			nexts.insert(nexts.end(), node_now.begin(), node_now.end());
		}
		
		/*******************
		 * Output
		 *******************/
		{
			cerr << "BEAM DEPTH: " << i << endl;
			cerr << "\tBEST EVALUE: " << best.Evaluate() << endl;
			cerr << "\tBEST SQUARE: " << best.ValidSquare() << endl;
			
			if((i + 1) % 50 == 0) {
				cerr << "[SUB RESULT]" << endl;
				cerr << "[PROCEDURE]" << endl;
				cerr << vtos(best.Procedure()) << endl;
				cerr << "[BOARD]" << endl;
				WriteBoard(best.board);
				cerr << "[SELECT POINT]" << endl;
				cerr << "x: "  << best.x << " y: " << best.y << endl;
				cerr << "[LOCK LINE]" << endl;
				cerr << "bottom: " << best.lock_bottom << " up: " << best.lock_up << endl;
				cerr << "left: " << best.lock_left << " right: " << best.lock_right << endl;
			}
			
			cerr << "\tTREE VOLUME: " << nexts.size() << endl;
		}
		
		now = Elites(nexts, BEAM_WIDTH, LUCKY_RATIO, no_update_times > 20);
		best = min(best, now.front());
		
		/*******************
		 * Arrange Params
		 *******************/
		if(best.Evaluate() == prev_evalue) {
			no_update_times++;
		} else {
			prev_evalue = best.Evaluate();
			no_update_times = 0;
			P1 = 0.01;
			P2 = 0.01;
			BEAM_WIDTH = 50;
			LUCKY_RATIO = 0.2;
		}
		
		cerr << "\tNO_UPDATE_TIMES: " << no_update_times << endl;
		
		if(no_update_times > 5 && no_update_times % 2 == 0) {
			P1 = min(P1 + 0.1, 1 - P1);
		}
		
		if(no_update_times > 5 && no_update_times % 2 == 1) {
			P2 = min(P2 + 0.1, 1 - P2);
		}
		
		if(no_update_times > 20) {
			BEAM_WIDTH = min(BEAM_WIDTH + 10, 200);
		}
		
		if(no_update_times > 20) {
			LUCKY_RATIO = 0.75;
		}
	}

	return best;
}

vector<Answer> Solver::Swaps(const Answer& answer) const
{
	vector<Answer> nexts;

	for(int i = 0; i < 4; i++) {
		int x = answer.x, y = answer.y;
		int nx = x + DX[i], ny = y + DY[i];
		
		if(nx <= answer.lock_left || answer.lock_right <= nx || ny <= answer.lock_bottom || answer.lock_up <= ny)
			continue;
		
		if(answer.Stupid(i))
			continue;

		nexts.push_back(answer.Swap(i));
	}

	return nexts;
}

vector<Answer> Solver::Selects(const Answer& answer, const double P2) const
{
	vector<Answer> nexts;

	if(answer.last_is_select)
		return nexts;
	
	if(answer.select_count == max_select)
		return nexts;
	
	for(int y = answer.lock_bottom + 1; y < answer.lock_up; y++) {
		for(int x = answer.lock_left + 1; x < answer.lock_right; x++) {
			if(x == answer.x && y == answer.y)
				continue;
			
			if(frand() > P2)
				continue;
			
			nexts.push_back(answer.Select(x, y));
		}
	}
	
	return nexts;
}

vector<Answer> Solver::Elites(vector<Answer>& answers, const int BEAM_WIDTH, const double LUCKY_RATIO, const bool dame)
{
	static const vector<int> the_magic_word = { 1, 1, 1, 5, 10, 20, 20, 20, 20, 20, 40, 40, 50, 50, 60, 60 };
	
	auto comp = [this](const Answer& a, const Answer& b) {
		if(a.ValidSquare() != b.ValidSquare())
			return a.ValidSquare() < b.ValidSquare();

		if(a.Evaluate() != b.Evaluate())
			return a.Evaluate() < b.Evaluate();

		return a.cost < b.cost;
	};
	
	sort(answers.begin(), answers.end(), comp);

	vector<Answer> results;
	for(int i = 0; results.size() < BEAM_WIDTH * (1 - LUCKY_RATIO) && i < answers.size(); i++) {
		bool ok = true;

		for(int j = 0; j < results.size(); j++) {
			int allow = 1;
			int s = min(answers[i].ValidSquare(), results[j].ValidSquare());
			
			for(int i = 1; !dame && i <= 16; i++) {
				if(s <= i * i) {
					allow = the_magic_word[i-1];
					break;
				}
			}
			
			if(Similarity(answers[i], results[j]) < allow) {
				if(answers[i].CompareTo(results[j]) < 0) {
					results[j] = answers[i];
				}

				ok = false;
				break;
			}
		}

		if(ok) {
			results.push_back(answers[i]);
		}
	}
	
	for(int W = BEAM_WIDTH * (1 - LUCKY_RATIO); W < answers.size() && results.size() < BEAM_WIDTH; ) {
		results.push_back(answers[W + rand() % (answers.size() - W)]);
	}

	sort(results.begin(), results.end(), comp);
	
	return results;
}

int Solver::Similarity(const Answer& a1, const Answer& a2)
{
	const int width = (int) a1.board[0].size();
	assert(width == WIDTH);
	static Board b1(HEIGHT, vector<int>(WIDTH));
	static Board b2(HEIGHT, vector<int>(WIDTH));
	
	for(int y = min(a1.lock_bottom, a2.lock_bottom) + 1; y < max(a1.lock_up, a2.lock_up); y++) {
		for(int x = min(a1.lock_left, a2.lock_left) + 1; x < max(a1.lock_right, a2.lock_right); x++) {
			int tx = a1.board[y][x] % width;
			int ty = a1.board[y][x] / width;
			
			b1[ty][tx] = x;
			b2[ty][tx] = y;
		}
	}
	
	int delta = 0;
	for(int y = min(a1.lock_bottom, a2.lock_bottom) + 1; y < max(a1.lock_up, a2.lock_up); y++) {
		for(int x = min(a1.lock_left, a2.lock_left) + 1; x < max(a1.lock_right, a2.lock_right); x++) {
			int tx = a2.board[y][x] % width;
			int ty = a2.board[y][x] / width;
			
			delta += abs(b1[ty][tx] - x) + abs(b2[ty][tx] - y);
		}
	}
	
	return delta;
}
