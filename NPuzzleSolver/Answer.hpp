#pragma once

#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>
#include "Settings.hpp"
#include "Simulater.hpp"
#include "Solver.hpp"
using namespace std;


class Answer
{
private:
	int select_cost, swap_cost;
	int last_direction;

	vector<string> all;
	string tail;
	
	int CalcEvaluationBasis();
	
public:
	int cost;
	int select_count;
	bool last_is_select;

	int x, y;
	int lock_bottom, lock_up, lock_left, lock_right;

	Board board;
	int evaluation_basis;
	bool solvable;

	Answer(const Board& board, int select_cost, int swap_cost, int x, int y);
	
	void Lock(int ox, int oy, int nx, int ny);
	
	int Evaluate() const;
	Answer Swap(int direction) const;
	Answer Select(int x, int y) const;
	vector<string> Procedure() const;
	
	int CompareTo(const Answer& other) const;
	bool operator < (const Answer& other) const;
	bool operator > (const Answer& other) const;
	
	bool Stupid(int direction) const;
	bool Solvable() const;
	bool Solvable(int max_select) const;
	bool Solvable(const Board& b, int ax, int ay) const;
	int Substitution(const vector<int>& subs, vector<int>& flag, int i) const;
	bool Correct_column(int column) const;
	bool Correct_row(int row) const;
	
	int ValidSquare() const;
};
