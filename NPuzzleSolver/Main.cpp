﻿#include "Main.hpp"

int main (int argc, char *argv[])
{
	auto begin_time = std::chrono::system_clock::now();
	
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);

	Board board = Shuffle(GenerateLinearBoard(8, 6), 1000);
	WriteBoard(board);

	int max_select = 6;
	int select_cost = 300;
	int swap_cost = 2;
	int WIDTH = 0, HEIGHT = 0;
	int time_limit = 1000000;

	if(argc == 1) {
		cin >> select_cost >> swap_cost;
		cin >> max_select;
		cin >> HEIGHT >> WIDTH;
		board = vector<vector<int>>(HEIGHT, vector<int>(WIDTH));
		for(int i = 0; i < HEIGHT; i++) {
			for(int j = 0; j < WIDTH; j++) {
				cin >> board[i][j];
			}
		}
		
		cin >> time_limit;
	}
	
	Solver solver(board, max_select, select_cost, swap_cost);
	Answer answer = solver.Solve(time_limit);

	cout << vtos(answer.Procedure()) << endl;

	WriteBoard(answer.board);

	int selected_cost = answer.select_count * select_cost;
	int selected_count = answer.select_count;
	int swapped_cost = answer.cost - answer.select_count * select_cost;
	int swapped_count = (answer.cost - answer.select_count * select_cost) / swap_cost;
	auto elapsed_time = chrono::system_clock::now() - begin_time;

	cerr << "Select: " << selected_cost << "(" << selected_count << ")" << endl;
	cerr << "Swap: " << swapped_cost << "(" << swapped_count << ")" << endl;
	cerr << "Time: " << chrono::duration_cast<chrono::milliseconds>(elapsed_time).count() / 10 << "(" << chrono::duration_cast<chrono::seconds>(elapsed_time).count()  << ")" << endl;
	cerr << "Total: " << answer.cost + chrono::duration_cast<chrono::milliseconds>(elapsed_time).count() / 10 << endl;
	cout << "__EOF__" << endl;
}

Board GenerateLinearBoard(int width, int height)
{
	Board board(height, vector<int>(width));

	for(int y = 0; y < height; y++) {
		for(int x = 0; x < width; x++) {
			board[y][x] = y * width + x;
		}
	}

	return board;
}

Board Shuffle(Board board, int shuffle_times)
{
	const int width = (int) board[0].size();
	const int height = (int) board.size();

	for(int i = 0; i < shuffle_times; i++) {
		int x = rand() % width, y = rand() % height;
		int nx = rand() % width, ny = rand() % height;

		swap(board[y][x], board[ny][nx]);
	}

	return board;
}
