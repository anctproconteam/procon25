#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <chrono>
#include "Settings.hpp"
#include "Solver.hpp"
#include "Answer.hpp"
using namespace std;

Board GenerateLinearBoard(int width, int height);
Board Shuffle(Board board, int shuffle_times);
