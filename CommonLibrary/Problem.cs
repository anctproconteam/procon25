﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procon.PPMLoader;

namespace Procon.CommonLibrary
{
    public class Problem
    {
        public int No { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int MaxSelect { get; private set; }
        public int SelectCost { get; private set; }
        public int SwapCost { get; private set; }
        public string Path { get; private set; }
        public Image Image { get; private set; }
        public Image[] SplitedImage { get; private set; }
        public string SizeString { get { return string.Format("{0}x{1}", Width, Height); } }
        public Problem(
            int No,
            int Width,
            int Height,
            int MaxSelect,
            int SelectCost,
            int SwapCost,
            string Path,
            Image Image,
            Image[] splitedImage
            )
        {
            this.No = No;
            this.Width = Width;
            this.Height = Height;
            this.MaxSelect = MaxSelect;
            this.SelectCost = SelectCost;
            this.SwapCost = SwapCost;
            this.Path = Path;
            this.Image = Image;
            this.SplitedImage = splitedImage;
        }
    }
    public class ProblemConstructor
    {
        public int No;
        public int Width;
        public int Height;
        public int MaxSelect;
        public int SelectCost;
        public int SwapCost;
        public string ImagePath;
        public Image image;
        public Image[] splitedImage;

        public ProblemConstructor()
        {
        }
        public ProblemConstructor(string header)
        {
            var scanner = new StringScanner(header);
            Width = scanner.ReadInt();
            Height = scanner.ReadInt();
            MaxSelect = scanner.ReadInt();
            SelectCost = scanner.ReadInt();
            SwapCost = scanner.ReadInt();
        }

        public Problem Construct()
        {
            return new Problem(
                No,
                Width,
                Height,
                MaxSelect,
                SelectCost,
                SwapCost,
                ImagePath,
                image,
                splitedImage
                );
        }
    }
}
