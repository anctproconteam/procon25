﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procon.CommonLibrary
{
    public class StringScanner
    {
        string str;
        int pos;

        public StringScanner(string str)
        {
            this.str = str;
            Reset();
        }

        bool toSkip(char c)
        {
            return str[pos] == ' ' || str[pos] == '\r' || str[pos] == '\n';
        }

        public void Reset()
        {
            pos = 0;
        }

        void SkipSpace()
        {
            while (pos < str.Length && toSkip(str[pos]))
            {
                pos++;
            }
        }

        string GetToken()
        {
            SkipSpace();
            var stringBuilder = new StringBuilder();
            while (pos < str.Length && !toSkip(str[pos]))
            {
                stringBuilder.Append(str[pos]);
                pos++;
            }
            return stringBuilder.ToString();
        }

        public int ReadInt()
        {
            return int.Parse(GetToken());
        }

        public char ReadChar(bool withSpace = false)
        {
            if (!withSpace)
                SkipSpace();
            return str[pos++];
        }

        public string ReadString()
        {
            return GetToken();
        }
    }
}
