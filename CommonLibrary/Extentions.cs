﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procon.CommonLibrary
{
    public struct IndexType<T>
    {
        public readonly int Index;
        public readonly T Value;

        public IndexType(int idx, T val){
            Index = idx;
            Value = val;
        }
    }
    public static class Extentions
    {
        static public int ToIntAs16(this char c)
        {
            if(char.IsDigit(c))
                return (int)(c - '0');
            return 10 + (int)(c - 'A');
        }
        static public IEnumerable<IndexType<T>> WithIndex<T>(this IEnumerable<T> ie)
        {
            int i = 0;
            return ie.Select(v => new IndexType<T>(i++, v)).ToArray();
        }

        static public bool IsInnerOf(this int val, int min, int max)
        {
            return min <= val && val <= max;
        }

        static public string GenerateString(this IEnumerable<char> source)
        {
            var builder = new StringBuilder();
            foreach (var v in source)
                builder.Append(v);
            return builder.ToString();
        }
    }
}
