﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procon.CommonLibrary
{
    public class OperationSequence
    {
        public readonly Operation[] Sequence;
        public readonly long Cost;
        public readonly long CostWithoutTime;
        private string answer = null;
        public OperationSequence(IEnumerable<Operation> sequence, long cost, long costWithoutTime, string answer)
        {
            Sequence = sequence.ToArray();
            Cost = cost;
            CostWithoutTime = costWithoutTime;
            this.answer = answer;
        }

        static public OperationSequence Parse(Problem problem, string str)
        {
            var seq = new List<Operation>();
            var scanner = new StringScanner(str);
            var selc = scanner.ReadInt();
            long cost = 0;
            for (int i = 0; i < selc; i++)
            {
                var f = scanner.ReadChar().ToIntAs16();
                var e = scanner.ReadChar().ToIntAs16();
                seq.Add(new SelectOperation(new Cell(f, e)));
                cost += problem.SelectCost;
                var n = scanner.ReadInt();
                for (int j = 0; j < n; j++)
                {
                    var d = scanner.ReadChar();
                    switch (d)
                    {
                        case 'L':
                            seq.Add(new ChangeOperation(Direction.Left));
                            break;
                        case 'R':
                            seq.Add(new ChangeOperation(Direction.Right));
                            break;
                        case 'U':
                            seq.Add(new ChangeOperation(Direction.Up));
                            break;
                        case 'D':
                            seq.Add(new ChangeOperation(Direction.Down));
                            break;
                        default:
                            break;
                    }
                    cost += problem.SwapCost;
                }
            }
            return new OperationSequence(seq, cost, cost, str);
        }

        public override string ToString()
        {
            return answer;
        }
    }
}
