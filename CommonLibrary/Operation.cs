﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procon.CommonLibrary
{
    public struct Cell
    {
        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public readonly int X;
        public readonly int Y;

        public override string ToString()
        {
            return "{" + X + ", " + Y + "}";
        }

        public static Cell operator +(Cell c1, Cell c2)
        {
            return new Cell(c1.X + c2.X, c1.Y + c2.Y);
        }
    }

    public enum Direction
    {
        Left, Right, Up, Down
    }

    public abstract class Operation
    {
    }

    public class ChangeOperation : Operation
    {
        public ChangeOperation(Direction d)
        {
            ChangeDirection = d;
        }
        public readonly Direction ChangeDirection;
    }

    public class SelectOperation : Operation
    {
        public SelectOperation(Cell s)
        {
            SelectCell = s;
        }
        public readonly Cell SelectCell;
    }
}
