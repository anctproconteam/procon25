﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageAnalysis
{
    class SquareSides
    {
        //public int top, bottom, left, right;
        public long top, left;
        public long bottom, right;

        public SquareSides()
        {
            top = bottom = left = right = 0;
        }

        public override string ToString()
        {
            return "Left: " + left + ", Top:" + top;
        }
    }
}
