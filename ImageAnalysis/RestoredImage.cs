﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Procon.PPMLoader;
using Procon.CommonLibrary;
using System.IO;

namespace ImageAnalysis
{
    public class RestoredImage
    {
        public int[,] GetBoard(String filename)
        {
            using (var stream = File.OpenRead(filename))
            {
                return GetBoard(stream);
            }
        }
        public int[,] GetBoard(Stream stream)
        {
            var image = ImageInstance.Load(stream);
            var pconst = new ProblemConstructor(image.Comment);
            var images = image.SplitImage(pconst.Width, pconst.Height);
            pconst.image = image;
            pconst.splitedImage = images;
            pconst.No = -1;
            var problem = pconst.Construct();
            return GetBoard(problem);
        }
        public int[,] GetBoard(Problem problem)
        {
            var images = problem.SplitedImage;

#if FALSE
            foreach (var i in images)
            {
                OpenCvSharp.CPlusPlus.Window.ShowImages(i);
                OpenCvSharp.CPlusPlus.Window.WaitKey();
            }
#endif

            //var eval = new EvaluationFunctionByEdge();
            var eval = new EvaluationFunctionByRGB(images);
            //var eval = new HybridFunctionByEdge();

            var solver = new GreedySearch(images, problem.Width, problem.Height, eval, false);

            try
            {
                solver.Run();
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString(), "Error");
                return null;
            }
            var result = solver.Result;
            return result;
            int[] map = new int[problem.Width * problem.Height];
            for (int x = 0; x < problem.Width; x++)
            {
                for (int y = 0; y < problem.Height; y++)
                {
                    var p = result[x, y];
                    var i = x + y * problem.Width;
                    map[result[x, y]] = i;
                }
            }
            for (int x = 0; x < problem.Width; x++)
            {
                for (int y = 0; y < problem.Height; y++)
                {
                    result[x, y] = map[x + y * problem.Width];
                }
            }
            return result;
        }

        public async Task<int[,]> GetBoardAsync(Problem problem)
        {
            var result = await Task<int[,]>.Run(
                () => { return GetBoard(problem); }
                );
            return result;
        }
    }
}
