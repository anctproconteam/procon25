﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageAnalysis
{
    static class LinqExtentions
    {
        static public IEnumerable<Tuple<T, T>> DirectProduct<T>(this IEnumerable<T> val)
        {
            return val.SelectMany(x => val.Select(y => Tuple.Create(x, y)));
        }
        static public IEnumerable<KeyValuePair<int, T>> WithIndex<T>(this IEnumerable<T> val)
        {
            int i = 0;
            return val.Select(v => new KeyValuePair<int, T>(i++, v)).ToArray();
        }
        static public IEnumerable<Tuple<T, T>> Double<T>(this IEnumerable<T> source)
        {
            var it = source.GetEnumerator();
            while (it.MoveNext())
            {
                T a = it.Current;
                if (!it.MoveNext())
                    throw new ArgumentException("need to be 偶数");
                T b = it.Current;
                yield return Tuple.Create(a, b);
            }
        }
    }
}
