﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCvSharp;
using OpenCvSharp.CPlusPlus;

namespace ImageAnalysis
{
    public class Utility
    {
        public static IplImage Canny(Mat image, int threshold1, int threshold2)
        {
            var src = image.ToIplImage();
            
            var x = image.ToCvMat();
            var dst = new IplImage(new CvSize(src.Width, src.Height), BitDepth.U8, 1);

            Cv.Canny(src, dst, threshold1, threshold2);
            return dst;
        }

        public static IplImage GetPartialImage(IplImage image, CvRect roi)
        {
            image.SetROI(roi);
            IplImage part = new IplImage(roi.Size, BitDepth.U8, image.NChannels);
            Cv.Copy(image, part);
            image.ResetROI();
            return part;
        }
    }
}
