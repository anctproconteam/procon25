using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using OpenCvSharp;
using OpenCvSharp.CPlusPlus;
using OpenCvSharp.Utilities;
namespace ImageAnalysis
{
    // @brief: ppmファイルを解析するクラス
    class PPM
    {
        public int splitingWidth;
        public int splitingHeight;
        public int selectedCost;
        public int exchangedCost;
        public int selectedAvailableNumber;
        public IplImage data;

        public PPM(String path)
        {
            //data = new Mat(path);
            data = new IplImage(path, LoadMode.AnyColor);
            using (var reader = new StreamReader(path))
            {
                // ==========
                // PPM Format
                // ==========
                // 1. P6
                // 2. # 4 2 ········分割数
                // 3. # 3···········選択可能回数
                // 4. # 150 20 ·····コスト変換レート
                // 5. 640 480 ······ピクセル数
                // 6. 255···········最大輝度

                // 最初の行は読み飛ばす
                var tmp = reader.ReadLine();
                tmp = reader.ReadLine();
                var tmps = tmp.Split(' ');
                splitingWidth = int.Parse(tmps[1]);
                splitingHeight = int.Parse(tmps[2]);
                tmp = reader.ReadLine();
                tmps = tmp.Split(' ');
                selectedAvailableNumber = int.Parse(tmps[1]);
                tmp = reader.ReadLine();
                tmps = tmp.Split(' ');
                selectedCost = int.Parse(tmps[1]);
                exchangedCost = int.Parse(tmps[2]);
            }
        }

        // https://github.com/shimat/opencvsharp/wiki/%5BCpp%5D-Accessing-Pixel
        // MatよりもMatOfByte3の方が画素へのアクセスが早いらしい
        public List<Mat> Split()
        {
            var ret = new List<Mat>();
            int height = data.Height / splitingHeight;
            int width = data.Width / splitingWidth;

            for (int i = 0; i < splitingHeight; i++)
            {
                for (int j = 0; j < splitingWidth; j++)
                {
                   // ret.Add(data[new CvRect(j * width, i * height, width, height)]);
                    ret.Add(Cv2.CvArrToMat(Utility.GetPartialImage(data, new CvRect(width * j, height * i, width, height))));
                }
            }

            return ret;
        }
    }
}
