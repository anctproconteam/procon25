﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Procon.PPMLoader;

namespace ImageAnalysis
{

    class EvaluateValues
    {
        SquareSides[,] values;
        int width;
        int height;

        public EvaluateValues(int w, int h)
        {
            width = w; height = h;
            values = new SquareSides[w * h, w * h];
            for (int i = 0; i < w * h; i++)
            {
                for (int j = 0; j < w * h; j++)
                {
                    values[i, j] = new SquareSides();
                }
            }
        }

        public SquareSides this[int a, int b]
        {
            get
            {
                return values[a, b];
            }
            set
            {
                values[a, b] = value;
            }
        }

        public void Update()
        {
            for (int i = 0; i < width * height; i++)
            {
                for (int j = 0; j < width * height; j++)
                {
                    if (i == j)
                        continue;
                    values[i, j].right = values[j, i].left;
                    values[i, j].bottom = values[j, i].top;
                }
            }
        }
    }

    

    abstract class Solver
    {
        protected bool isDebug;
        protected int height;
        protected int width;
        public Image[] images;
        public EvaluateValues evaluateValues;
        protected int[,] result;
        protected long maxCost;

        public int Height
        {
            get { return height; }
        }

        public int Width
        {
            get { return width; }
        }

        public int[,] Result
        {
            get { return result; }
        }

        public Solver(Image[] images, int splitingWidth, int splitingHeight, EvaluationBase evaluation, bool isDebug)
        {
            this.isDebug = isDebug;
            this.images = images;
            width = splitingWidth;
            height = splitingHeight;

            evaluateValues = new EvaluateValues(splitingWidth, splitingHeight);

            foreach (var v in images.WithIndex().DirectProduct().Where(v => v.Item1.Key != v.Item2.Key))
            {
                evaluateValues[v.Item1.Key, v.Item2.Key] = evaluation.Evaluate(this, v.Item1.Key, v.Item2.Key);
            }
            evaluateValues.Update();
        }

        public abstract void Run();
    }
}
