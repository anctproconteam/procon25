﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Procon.PPMLoader;

namespace ImageAnalysis
{
    static class ImageExtentions
    {
        public static PixelDistance[,] HorizontalCalculation(this PixelDistance[,] table)
        {
            int width = table.GetLength(0);
            int height = table.GetLength(1);
            var ret = new PixelDistance[width - 1, height];

            for (int x = 0; x < width - 1; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    ret[x, y] = table[x, y] - table[x + 1, y];
                }
            }
            return ret;
        }

        public static PixelDistance[,] VerticalCalculation(this PixelDistance[,] table) 
        {
            int width = table.GetLength(0);
            int height = table.GetLength(1);
            var ret = new PixelDistance[width, height - 1];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height - 1; y++)
                {
                    ret[x, y] = table[x, y] - table[x, y + 1];
                }
            }

            return ret;
        }

        //public static Mat ToMat(this HorizontalCombinedImage image)
        //{
        //    var ret = new Mat(image.Width, image.Height);

        //    for (int x = 0; x < image.Width; x++)
        //    {
        //        for (int y = 0; y < image.Height; y++)
        //        {
        //            ret[x, y] = (PixelDistance)image[x, y];
        //        }
        //    }

        //    return ret;
        //}

        //public static Mat ToMat(this VerticalCombinedImage image)
        //{
        //    var ret = new Mat(image.Width, image.Height);

        //    for (int x = 0; x < image.Width; x++)
        //    {
        //        for (int y = 0; y < image.Height; y++)
        //        {
        //            ret[x, y] = (PixelDistance)image[x, y];
        //        }
        //    }

        //    return ret;
        //}

        //public static double MedHorizontalValue(this Mat mat, double p)
        //{
        //    double ret = 0L;
        //    int y = mat.Height / 2 - 1;
        //    for (int x = 0; x < mat.Width; x++)
        //    {
        //        ret += p * Calc(mat[x, y], mat[x, y + 1]);
        //    }

        //    return ret;
        //}

        //public static double MedVerticalValue(this Mat mat, double p)
        //{
        //    double ret = 0L;
        //    int x = mat.Width / 2 - 1;
        //    for (int y = 0; y < mat.Height; y++)
        //    {
        //        ret += p * Calc(mat[x, y], mat[x + 1, y]);
        //    }

        //    return ret;
        //}

        //public static long Calc(PixelDistance a, PixelDistance b)
        //{
        //    Func<float, int> f = (v) => (int)Math.Pow(v, 1);
        //    return (int)(
        //        f(Math.Abs(a.R - b.R))
        //        + f(Math.Abs(a.G- b.G))
        //        + f(Math.Abs(a.B- b.B))
        //        );
        //}
    }
}
