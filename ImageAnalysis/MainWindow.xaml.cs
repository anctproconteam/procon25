using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Diagnostics;

using Procon.PPMLoader;
using Procon.CommonLibrary;
//using OpenCvSharp.CPlusPlus;

namespace ImageAnalysis
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();


#if DEBUG
            String path = @"images/prob10.ppm";
            var image = ImageInstance.LoadByFile(path);
            var pconst = new ProblemConstructor(image.Comment);
            var images = image.SplitImage(pconst.Width, pconst.Height);
            pconst.image = image;
            pconst.splitedImage = images;
            pconst.No = -1;
            var problem = pconst.Construct();

#if FALSE
            foreach (var i in images)
            {
                OpenCvSharp.CPlusPlus.Window.ShowImages(i);
                OpenCvSharp.CPlusPlus.Window.WaitKey();
            }
#endif

            //var eval = new EvaluationFunctionByEdge();
            var eval = new EvaluationFunctionByRGB(images);
            //var eval = new HybridFunctionByEdge();


            GreedySearch solver;

            try
            {
                solver = new GreedySearch(images, problem.Width, problem.Height, eval, false);
                solver.Run();
            }
            catch (Exception e)
            {
                var str = e.ToString();
                Debug.WriteLine(str);
                throw e;
            }

            this.Width = image.Width;
            this.Height = image.Height;
           
            grid1.Height = image.Height;
            grid1.Width = image.Width;

            for (int i = 0; i < solver.Width; i++)
            {
                grid1.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < solver.Height; i++)
            {
                grid1.RowDefinitions.Add(new RowDefinition());
            }
            for (int i = 0; i < solver.Height; i++)
            {
                for (int j = 0; j < solver.Width; j++)
                {
                    var p = solver.Result[j, i];

                    var panel = new Canvas();
                    var x = new System.Windows.Controls.Image();
                    string str = "";
                    if (j != 0)
                    {
                        var pp = solver.Result[j - 1, i];
                        str += "Left:" + solver.evaluateValues[p, pp].left.ToString() + "\n";
                    }
                    if (i != 0)
                    {
                        var pp = solver.Result[j, i - 1];
                        str += "Top:" + solver.evaluateValues[p, pp].top.ToString() + "\n";
                    }
                    if (j != solver.Width - 1)
                    {
                        var pp = solver.Result[j + 1, i];
                        str += "Right:" + solver.evaluateValues[p, pp].right.ToString() + "\n";
                    }
                    if (i != solver.Height - 1)
                    {
                        var pp = solver.Result[j, i + 1];
                        str += "Bottom:" + solver.evaluateValues[p, pp].bottom.ToString() + "\n";
                    }
                    panel.MouseEnter += (e, obj) =>
                    {
                        Text.Text = str;
                    };
                    //panel.ToolTip = str;
                    panel.SetValue(Grid.RowProperty, i);
                    panel.SetValue(Grid.ColumnProperty, j);
                    x.Source = solver.images[solver.Result[j, i]].ToBitmap();
                    panel.Children.Add(x);
                    grid1.Children.Add(panel);
                }
            }



            //Update(solver, image);
#endif
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
