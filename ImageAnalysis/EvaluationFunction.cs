﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Procon.PPMLoader;

namespace ImageAnalysis
{
    class EvaluationBase
    {
        virtual protected Image GetMat(Solver bt, int p)
        {
            throw new NotImplementedException();
        }
        virtual public SquareSides Evaluate(Solver bt, int a, int b)
        {
            throw new NotImplementedException();
            //return Evaluate(bt, a, b));
        }
        virtual protected SquareSides Evaluate(Image a, Image b)
        {
            throw new NotImplementedException();
        }
    }

    class EvaluationFunctionByRGB : EvaluationBase
    {
        double[] PARAM;
        int DIFNUM;
        List<List<PixelDistance[,]>> horizontalTables, verticalTables;

        public EvaluationFunctionByRGB(Image[] images)
        {
            PARAM = new double[]{1, 0.15, 0.01, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            DIFNUM = Math.Min(2, Math.Min(images[0].Width, images[0].Height));

            horizontalTables = new List<List<PixelDistance[,]>>();
            verticalTables = new List<List<PixelDistance[,]>>();

            horizontalTables.Add(new List<PixelDistance[,]>());
            verticalTables.Add(new List<PixelDistance[,]>());

            foreach (var image in images)
            {
                PixelDistance[,] table = new PixelDistance[image.Width, image.Height];
                for (int x = 0; x < image.Width; x++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        table[x, y] = (PixelDistance)image[x, y];
                    }
                }
                horizontalTables[0].Add(table);
                verticalTables[0].Add(table);
            }

            for (int c = 0; c < DIFNUM; c++)
            {
                var h = new List<PixelDistance[,]>();
                var v = new List<PixelDistance[,]>();

                foreach (var image in horizontalTables[horizontalTables.Count - 1])
                {
                    h.Add(image.HorizontalCalculation());
                }

                foreach (var image in verticalTables[verticalTables.Count - 1])
                {
                    v.Add(image.VerticalCalculation());
                }

                horizontalTables.Add(h);
                verticalTables.Add(v);
            }
        }

        protected override Image GetMat(Solver bt, int p)
        {
            return bt.images[p];
        }


        public override SquareSides Evaluate(Solver bt, int v, int w)
        {
            //Image a = bt.images[v];
            //Image b = bt.images[w];

            int width = verticalTables[0][v].GetLength(0);
            int height = horizontalTables[0][v].GetLength(1);

            SquareSides ss = new SquareSides();
            for (int k = 0; k < width; k++)
            {
                for(int i = 0; i < DIFNUM; i++) 
                {
                    var color1 = verticalTables[i][v][k, 0];
                    var color2 = verticalTables[i][w][k, verticalTables[i][w].GetLength(1) - 1];
                    ss.top += (long)(PARAM[i] * Calc(color1, color2));
                }
            }

            for (int k = 0; k < height; k++)
            {
                for (int i = 0; i < DIFNUM; i++)
                {
                    var color1 = horizontalTables[i][v][0, k];
                    var color2 = horizontalTables[i][w][horizontalTables[i][w].GetLength(0) - 1, k];
                    ss.left += (long)(PARAM[i] * Calc(color1, color2));
                }
            }

            ss.top /= width;
            ss.top = (long)Math.Pow(ss.top, 3);
            ss.left /= height;
            ss.left = (long)Math.Pow(ss.left, 3);
            
            return ss;
        }
        
        public long Calc(Pixel a, Pixel b)
        {
            Func<float, int> f = (v) => (int)Math.Pow(v, 1);
            return (int)(
                f(Math.Abs(a.R - b.R))
                + f(Math.Abs(a.G- b.G))
                + f(Math.Abs(a.B- b.B))
                );
        }

        public long Calc(PixelDistance a, PixelDistance b)
        {
            Func<float, int> f = (v) => (int)Math.Pow(v, 1);
            return (int)(
                f(Math.Abs(a.R - b.R))
                + f(Math.Abs(a.G- b.G))
                + f(Math.Abs(a.B- b.B))
                );
        }
    }
}