﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Procon.PPMLoader;

namespace ImageAnalysis
{
    public struct Vec2i
    {
        public int Item0;
        public int Item1;

        public Vec2i(int a, int b)
        {
            Item0 = a;
            Item1 = b;
        }
        
    }

    // 最初の選択セルは全探索、その後は選択セルの隣4セルの中から評価値が一番高いものを決定する。
    class GreedySearch : Solver
    {

        public GreedySearch(Image[] images, int splitingWidth, int splitingHeight, EvaluationBase evaluation, bool isDebug)
            : base(images, splitingWidth, splitingHeight, evaluation, isDebug)
        {
        }

        public override void Run()
        {
            int StepCount = 100;
            maxCost = long.MaxValue;
            for (int c = 0; c < StepCount; c++)
            {
                for (int p = 0; p < width * height; p++)
                {
                    var random = new System.Random();
                    int x = width / 2 + random.Next(-width / 2, width / 2);
                    int y = height / 2 + random.Next(-height / 2, height / 2);

                    var table = new int[width, height];
                    for (int i = 0; i < height; i++) for (int j = 0; j < width; j++) table[j, i] = -1;
                    var used = new bool[width * height];
                    table[x, y] = p;
                    used[p] = true;
                    var r = BFS(x, y, table, used);

                    if (r.Item1 == null) continue;

                    results.Add(r.Item1.Clone() as int[,]);

                    if (maxCost > r.Item2)
                    {
                        maxCost = r.Item2;
                        result = r.Item1;
                    }
                }
            }
        }

        public Tuple<int[,], long> BFS(int x, int y, int[,] table, bool[] used)
        {
            var que = new Queue<Tuple<Vec2i, long>>();

            // right, bottom, left, top
            var dx = new int[] { 1, 0, -1, 0 };
            var dy = new int[] { 0, 1, 0, -1 };

            Func<int, int, bool> IsInTable = (nx, ny) =>
            {
                return nx >= 0 && ny >= 0 && nx < width && ny < height;
            };

            que.Enqueue(Tuple.Create<Vec2i, long>(new Vec2i(x, y), 0));

            while (que.Count != 0)
            {
                var q = que.Dequeue();

                if (q.Item2 > maxCost)
                {
                    return Tuple.Create<int[,], long>(null, long.MaxValue);
                }

                int best_dir = -1;
                int next_pos = -1;
                long best_evaluation = long.MaxValue;

                //Console.WriteLine("(x, y) == ({0}, {1})", q.Item1.Item0, q.Item1.Item1);

                for (int d = 0; d < 4; d++)
                {
                    int pos = -1;
                    long evaluation = long.MaxValue;
                    var nx = q.Item1.Item0 + dx[d];
                    var ny = q.Item1.Item1 + dy[d];

                    if (!IsInTable(nx, ny) || table[nx, ny] != -1)
                    {
                        continue;
                    }

                    for (int p = 0; p < width * height; p++)
                    {
                        if (used[p]) continue;

                        long v = 0L;
                        int w = 0;

                        for (int n = 0; n < 4; n++)
                        {
                            int sx = nx + dx[n];
                            int sy = ny + dy[n];

                            if (!IsInTable(sx, sy) || table[sx, sy] == -1)
                            {
                                continue;
                            }

                            w++;

                            switch (n)
                            {
                                case 0:
                                    v += evaluateValues[p, table[sx, sy]].right;
                                    break;
                                case 1:
                                    v += evaluateValues[p, table[sx, sy]].bottom;
                                    break;
                                case 2:
                                    v += evaluateValues[p, table[sx, sy]].left;
                                    break;
                                case 3:
                                    v += evaluateValues[p, table[sx, sy]].top;
                                    break;
                            }
                        }

                        v /= w;

                        if (v < evaluation)
                        {
                            evaluation = v;
                            pos = p;
                        }
                    }

                    if (evaluation < best_evaluation)
                    {
                        best_evaluation = evaluation;
                        next_pos = pos;
                        best_dir = d;
                    }
                }

                // 終了場所
                if (next_pos == -1 || best_dir == -1 || best_evaluation == long.MaxValue)
                {
                    bool ok = true;
                    for (int i = 0; i < height; i++)
                    {
                        for (int j = 0; j < width; j++)
                        {
                            if (table[j, i] == -1) continue;

                            for (int d = 0; d < 4; d++)
                            {
                                if (!ok) break;
                                int nx = j + dx[d];
                                int ny = i + dy[d];
                                if (!IsInTable(nx, ny)) continue;
                                if (table[nx, ny] == -1)
                                {
                                    que.Enqueue(Tuple.Create<Vec2i, long>(new Vec2i(j, i), q.Item2));
                                    ok = false;
                                }
                            }
                        }
                    }

                    if (ok)
                    {
                        return Tuple.Create<int[,], long>(table.Clone() as int[,], q.Item2);
                    }
                    else
                    {
                        continue;
                    }
                }

                int next_x = q.Item1.Item0 + dx[best_dir];
                int next_y = q.Item1.Item1 + dy[best_dir];
                long next_e = q.Item2 + best_evaluation;
                table[next_x, next_y] = next_pos;
                used[next_pos] = true;
                que.Enqueue(Tuple.Create<Vec2i, long>(new Vec2i(next_x, next_y), next_e));
            }

            return Tuple.Create<int[,], long>(null, long.MaxValue);
        }
    }

    class BackTracking : Solver
    {
        public BackTracking(Image[] images, int splitingWidth, int splitingHeight, EvaluationBase evaluation, bool isDebug)
            : base(images, splitingWidth, splitingHeight, evaluation, isDebug)
        {
        }


        public override void Run()
        {
            maxCost = long.MaxValue;
            for (int i = 0; i < width * height; i++)
            {
                int[,] table = new int[width, height];
                table[0, 0] = i;
                bool[] used = new bool[height * width];
                used[i] = true;
                var r = OuterDFS(1, table, used, 0);

                if (r.Item1 == null)
                    continue;

                results.Add(r.Item1);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Console.Write(" {0}", r.Item1[x, y]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("---");

                if (maxCost > r.Item2)
                {
                    maxCost = r.Item2;
                    result = r.Item1;
                }
            }
        }

        public Tuple<int[,], long> OuterDFS(int pos, int[,] table, bool[] used, long cost)
        {
            //Console.WriteLine(pos);
            if (cost > maxCost)
            {
                return new Tuple<int[,], long>(null, long.MaxValue);
            }
            if (pos >= width * height)
            {
                return Tuple.Create(table.Clone() as int[,], cost);
            }
            var result = InnerDFS(pos, table, used, cost);
            return OuterDFS(pos + 1, result.Item1, result.Item2, result.Item3);
        }

        public Tuple<int[,], bool[], long> InnerDFS(int pos, int[,] t, bool[] u, long cost)
        {
            const int maxDepth = 3;
            Func<int, int> getY = (p) =>
            {
                return p / width;
            };
            Func<int, int> getX = (p) =>
            {
                return p % width;
                //if (getY(p) % 2 == 0)
                //{
                //    return p % width;
                //}
                //else
                //{
                //    return width - 1 - p % width;
                //}
            };
            int[,] resultTable = null;
            bool[] resultUsed = null;
            int[,] table = t.Clone() as int[,];
            bool[] used = u.Clone() as bool[];
            int[] history = new int[maxDepth];
            int res = 0;
            Action<int, int, long> func = null;
            long ans = long.MaxValue;
            func = (i, p, c) =>
            {
                //枝刈り
                if (ans < c || (maxCost < c && ans != long.MaxValue))
                {
                    return;
                }
                //終了
                if (i == maxDepth || width * height <= p)
                {
                    if (ans > c)
                    {
                        ans = c;
                        //resultTable = table.Clone() as int[,];
                        //resultUsed = used.Clone() as bool[];
                        res = history[0];
                    }
                    return;
                }
                //通常
                int x = getX(p);
                int y = getY(p);
                var s = used.WithIndex().Where(v => !v.Value).Select(v => v.Key)
                    .Select(v =>
                    {
                        long cc = 0;
                        //if (y % 2 == 0 && x > 0)
                        //    cc += evaluateValues[v, table[x - 1, y]].left;
                        //if (y % 2 == 1 && x < width - 1)
                        //    cc += evaluateValues[v, table[x + 1, y]].right;
                        if (x > 0)
                            cc += evaluateValues[v, table[x - 1, y]].left;
                        if (y > 0)
                            cc += evaluateValues[v, table[x, y - 1]].top;
                        return new KeyValuePair<int, long>(v, cc);
                    }
                ).OrderBy(v => v.Value);//.Take(4);
                foreach (var val in s)
                {
                    table[x, y] = val.Key;
                    used[val.Key] = true;
                    history[i] = val.Key;
                    func(i + 1, p + 1, cost + val.Value);
                    used[val.Key] = false;
                }
            };
            func(0, pos, cost);
            resultTable = table.Clone() as int[,];
            int xx = getX(pos); int yy = getY(pos);
            resultTable[xx, yy] = res;
            resultUsed = used.Clone() as bool[];
            resultUsed[res] = true;
            var ccc = cost;
            if (xx > 0)
                ccc += evaluateValues[res, table[xx - 1, yy]].left;
            if (yy > 0)
                ccc += evaluateValues[res, table[xx, yy - 1]].top;
            return Tuple.Create(resultTable, resultUsed, ccc);
        }
    }
}
