﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procon.CommonLibrary;

namespace ImageAnalysis
{
    class ExpandableBoard<T>
    {
        T[,] data;
        public readonly int Width;
        public readonly int Height;

        int minx = 0;
        int miny = 0;
        int maxx = 0;
        int maxy = 0;
        
        public ExpandableBoard(int width, int height, T fill_value)
        {
            Width = width;
            Height = height;
            data = new T[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    data[x, y] = fill_value;
                }
            }
        }

        public T this[int x, int y]
        {
            get
            {
                if (!isEnableColumn(x) || !isEnableRow(y))
                {
                    throw new IndexOutOfRangeException();
                }
                if (!x.IsInnerOf(minx, maxx) && !y.IsInnerOf(miny, maxy))
                {
                    return default(T);
                }
                return data[(x + Width) % Width, (y + Height) % Height];
            }
            set
            {
                if (!isEnableColumn(x) || !isEnableRow(y))
                {
                    throw new IndexOutOfRangeException();
                }
                minx = Math.Min(x, minx);
                maxx = Math.Max(x, maxx);
                miny = Math.Min(y, miny);
                maxy = Math.Max(y, maxy);
                data[(x + Width) % Width, (y + Height) % Height] = value;
            }
        }

        public bool isEnableColumn(int v)
        {
            var k = maxx - minx + 1;
            var remain = Width - k;
            return v.IsInnerOf(minx - remain, maxx + remain);
        }
        public bool isEnableRow(int v)
        {
            var k = maxy - miny + 1;
            var remain = Height - k;
            return v.IsInnerOf(miny - remain, maxy + remain);
        }

        public T[,] ToArray()
        {
            T[,] ret = new T[Width, Height];
            for (int x = minx; x <= maxx; x++)
            {
                for (int y = miny; y <= maxy; y++)
                {
                    ret[x - minx, y - miny] = data[(x + Width) % Width, (y + Height) % Height];
                }
            }
            return ret;
        }

        public bool IsInArea(int x, int y) 
        {
            return isEnableColumn(x) && isEnableRow(y);
        }
    }
}
