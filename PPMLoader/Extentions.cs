﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procon.PPMLoader
{
    static public class Extentions
    {
        public static bool ReadConstant(this BinaryReader reader, params byte[] arg)
        {
            foreach (var v in arg)
            {
                if (reader.ReadByte() != v)
                    return false;
            }
            return true;
        }
        public static bool IsSeparator(this byte b)
        {
            return b == 0x9 || b == 0x0a || b == 0x0d || b == 0x20;
        }
        public static bool IsLf(this byte b)
        {
            return b == 0x0a;
        }
        public static int ReadInt(this BinaryReader reader)
        {
            return int.Parse(reader.ReadToSeparator().GenerateString());
        }

        public static bool ReadSeparator(this BinaryReader reader)
        {
            return reader.ReadByte().IsSeparator();
        }
        public static byte[] ReadToSeparator(this BinaryReader reader)
        {
            var l = new List<byte>();
            while (true)
            {
                var b = reader.ReadByte();
                if (b.IsSeparator())
                    break;
                l.Add(b);
            }
            return l.ToArray();
        }
        public static byte[] ReadToLF(this BinaryReader reader)
        {
            var l = new List<byte>();
            while (true)
            {
                var b = reader.ReadByte();
                if (b == 0x0a)
                    break;
                l.Add(b);
            }
            return l.ToArray();
        }

        public static string GenerateString(this byte[] arg)
        {
            var builder = new StringBuilder();
            foreach (var v in arg)
            {
                builder.Append((char)v);
            }
            return builder.ToString();
        }
    }
}
