﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Procon.CommonLibrary;

namespace Procon.PPMLoader
{
    public class ImageInstance : Image
    {
        private ImageInstance(){
        }

        int width, height, maxBrightness;
        string comment;
        Pixel[,] data;

        public override int Width
        {
            get { return width; }
        }

        public override int Height
        {
            get { return height; }
        }

        public override int MaxBrightness
        {
            get { return maxBrightness; }
        }

        public override string Comment
        {
            get { return comment; }
        }

        public override Pixel this[int x, int y]
        {
            get
            {
                if (!x.IsInnerOf(0, Width) || !y.IsInnerOf(0, Height))
                {
                    throw new IndexOutOfRangeException();
                }
                return data[x, y];
            }
        }
        
        static public Image Load(String path)
        {
            using (var filestream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new BinaryReader(filestream, Encoding.UTF8))
                {
                    if (!reader.ReadConstant(0x50, 0x36))
                        throw new InvalidDataException();
                    if (!reader.ReadSeparator())
                        throw new InvalidDataException();
                    string comment = "";
                    int count = 0;
                    while (count < 3)
                    {
                        reader.ReadByte();
                        comment += reader.ReadToLF().GenerateString();
                        count++;
                    }
                    int width = reader.ReadInt();
                    int height = reader.ReadInt();
                    int maxBrightness = reader.ReadInt();

                    var data = new Pixel[width, height];

                    for (int i = 0; i < width * height; i++)
                    {
                        var r = reader.ReadByte();
                        var g = reader.ReadByte();
                        var b = reader.ReadByte();
                        data[i % width, i / width] = new Pixel(r, g, b);
                    }
                    var im = new ImageInstance();
                    im.width = width;
                    im.height = height;
                    im.maxBrightness = maxBrightness;
                    im.data = data;
                    im.comment = comment;
                    return im;
                }
            }
        }


    }
}
