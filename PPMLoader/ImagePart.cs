﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Procon.CommonLibrary;

namespace Procon.PPMLoader
{
    public class ImagePart : Image
    {
        Image baseImage;
        int x, y, width, height;

        public ImagePart(Image baseImage, int x, int y, int width, int height)
        {
            this.baseImage = baseImage;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public override int Width
        {
            get { return width; }
        }

        public override int Height
        {
            get { return height; }
        }

        public override int MaxBrightness
        {
            get { return baseImage.MaxBrightness; }
        }

        public override string Comment
        {
            get { return baseImage.Comment; }
        }

        public override Pixel this[int xx, int yy]
        {
            get
            {
                if (!xx.IsInnerOf(x, x + width) || !yy.IsInnerOf(y, y + height))
                    throw new IndexOutOfRangeException();
                return baseImage[x + xx, y + yy];
            }
        }
    }
}
