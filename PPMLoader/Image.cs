﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Procon.CommonLibrary;
using System.Windows.Media;

namespace Procon.PPMLoader
{
    public struct Pixel
    {
        public Pixel(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }
        public byte R;
        public byte G;
        public byte B;
    }
    public abstract class Image
    {
        public abstract int Width { get; }
        public abstract int Height { get; }
        public abstract int MaxBrightness { get; }
        public abstract string Comment { get; }

        private BitmapSource CreateBitmap()
        {
            var bitmap = new WriteableBitmap(Width, Height, 96, 96, PixelFormats.Bgr24, null);
            var count = Width * Height;
            var buffer = new byte[Width * Height * 3];
            for (int i = 0; i < count; i++)
            {
                var pix = this[i % Width, i / Width];
                buffer[i * 3 + 0] = pix.B;
                buffer[i * 3 + 1] = pix.G;
                buffer[i * 3 + 2] = pix.R;
            }
            bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, Width, Height), buffer, Width * 3, 0);
            return bitmap;
        }

        public abstract Pixel this[int x, int y]
        {
            get;
        }

        public Image[] SplitImage(int column, int row)
        {
            var buf = new Image[column * row];
            var w = Width / column;
            var h = Height / row;
            for (int x = 0; x < column; x++)
            {
                for (int y = 0; y < row; y++)
                {
                    buf[x + y * column] = new ImagePart(this, x * w, y * h, w, h);
                }
            }
            return buf;
        }

        private BitmapSource cache;
        public BitmapSource ToBitmap()
        {
            if (cache == null)
            {
                cache = CreateBitmap();
            }
            return cache;
        }
    }
}
