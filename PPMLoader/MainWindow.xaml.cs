﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Procon.PPMLoader;

namespace PPMLoader
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {

                var img = ImageInstance.Load(@"images/prob01.ppm");
                Root.Children.Add(
                    new System.Windows.Controls.Image()
                    {
                        Source = img.SplitImage(3, 3)[0, 0].ToBitmap(),
                    }
                    );
            }
            catch (Exception e)
            {
            }
        }
    }
}
