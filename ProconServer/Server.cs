﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Procon.CommonLibrary;

namespace ProconServer
{
    public class Server
    {
        readonly Procon.CommonLibrary.Problem[] Problems;
        object lockObj = new object();
        HttpListener httpListener;
        Dictionary<string, Action<HttpListenerRequest, HttpListenerResponse>> rooting = new Dictionary<string, Action<HttpListenerRequest, HttpListenerResponse>>();

        public Server(Procon.CommonLibrary.Problem[] problems)
        {
            this.Problems = problems;
            httpListener = new HttpListener();
            Initialize();
        }

        void Initialize()
        {
            rooting["favicon.ico"] = (req, res) =>
            {
                var reader = new StreamReader(req.InputStream);
                var writer = new StreamWriter(res.OutputStream);
                string received = reader.ReadToEnd();
                writer.Write(received);
                writer.Flush();
            };
            rooting["problem"] = (req, res) =>
            {
                var reader = new StreamReader(req.InputStream);
                var writer = new StreamWriter(res.OutputStream);
                var url = req.RawUrl;
                var filename = url.Skip(1 + 8).TakeWhile(c => c != '/').GenerateString();
                Console.WriteLine("Filename: {0}", filename);
                var regex = new Regex("prob(..)\\.ppm");
                var match = regex.Match(filename);
                if (match.Success)
                {
                    var id = int.Parse(match.Groups[1].Value);
                    Console.WriteLine("Id: {0}", id);
                    var file = Problems.First(p => p.No == id).Path;
                    res.ContentType = "image/ppm";
                    var data = File.ReadAllBytes(file);
                    res.OutputStream.Write(data, 0, data.Count());
                    res.OutputStream.Flush();
                }
                else
                {
                    string received = reader.ReadToEnd();
                    res.StatusCode = 404;
                    writer.Write(received);
                    writer.Flush();
                }
            };
            rooting["SubmitAnswer"] = (req, res) =>
            {
                var reader = new StreamReader(req.InputStream);
                var writer = new StreamWriter(res.OutputStream);
                try
                {
                    var str = reader.ReadToEnd();
                    Console.WriteLine("--");
                    Console.WriteLine(str);
                    Console.WriteLine("--");
                }catch(Exception e){
                }
                writer.Flush();
            };

        }

        public void Start()
        {
            lock (lockObj)
            {
                httpListener.Prefixes.Add("http://+:80/");
                httpListener.Start();
                httpListener.BeginGetContext(OnRequested, null);
            }
        }

        void OnRequested(IAsyncResult result)
        {
            lock (lockObj)
            {
                var listener = httpListener;
                if (!listener.IsListening)
                    return;
                var ctx = listener.EndGetContext(result);
                HttpListenerRequest req = null;
                HttpListenerResponse res = null;
                StreamReader reader = null;
                StreamWriter writer = null;

                try
                {
                    req = ctx.Request;
                    res = ctx.Response;

                    reader = new StreamReader(req.InputStream);
                    writer = new StreamWriter(res.OutputStream);

                    var url = req.RawUrl;
                    var method = url.Skip(1).TakeWhile(c => c != '/').GenerateString();
                    Console.WriteLine("Request: {0}", url);
                    Console.WriteLine("Method: {0}", method);

                    if (rooting.ContainsKey(method))
                    {
                        rooting[method](req, res);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    try
                    {
                        if (null != writer) writer.Close();
                        if (null != reader) reader.Close();
                        if (null != res) res.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                httpListener.BeginGetContext(OnRequested, null);
            }
        }
    }
}
