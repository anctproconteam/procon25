﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProconServer
{
    /// <summary>
    /// ServerInfomation.xaml の相互作用ロジック
    /// </summary>
    public partial class ServerInfomation : Window
    {
        Server server;
        public ServerInfomation(Procon.CommonLibrary.Problem[] p)
        {
            InitializeComponent();
            server = new Server(p);
            server.Start();
        }
    }
}
