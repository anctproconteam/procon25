﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProconClient;
using Procon.CommonLibrary;

namespace ProconServer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Problem> problems = new List<Problem>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.FilterIndex = 1;
            openFileDialog.Filter = "all|*.*";
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog().Value)
            {
                foreach (var filename in openFileDialog.FileNames)
                {
                    var ppm = Procon.PPMLoader.ImageInstance.LoadByFile(filename);
                    var pconst = new Procon.CommonLibrary.ProblemConstructor(ppm.Comment)
                    {
                        No = problems.Count() + 1,
                        ImagePath = filename,
                        image = ppm,
                    };
                    pconst.splitedImage = ppm.SplitImage(pconst.Width, pconst.Height);
                    var problem = pconst.Construct();
                    var pdata = new ProblemSolveData(problem);
                    problems.Add(problem);
                    var li = new ProblemListItem(pdata);
                    Images.Children.Add(li);
                }
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            var si = new ServerInfomation(problems.ToArray());
            si.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var window = new ProconClient.MainWindow();
            window.Show();
        }
    }
}
