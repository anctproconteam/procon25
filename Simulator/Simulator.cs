﻿using Procon.CommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProconSimulator
{
    class Simulator
    {
        List<Operation> seaquence;
        public IEnumerable<Operation> Seaquence
        {
            get { return seaquence; }
        }
        int pos = 0;

        public int Pos { get { return pos; } set { SetPosition(Pos); } }
        public Cell SelectCell { get; private set; }

        int[,] initBoard;
        int[,] board;

        public int[,] Board
        {
            get { return board.Clone() as int[,]; }
        }

        public int this[Cell c]
        {
            get
            {
                var x = c.X; var y = c.Y;
                return board[x, y];
            }
            set
            {
                var x = c.X; var y = c.Y;
                board[x, y] = value;
            }
        }
        public int this[int x, int y]
        {
            get
            {
                return board[x, y];
            }
            set
            {
                board[x, y] = value;
            }
        }


        public Simulator(int[,] board, IEnumerable<Operation> seq)
        {
            initBoard = board.Clone() as int[,];
            seaquence = seq.ToList();
            Reset();
        }

        void Reset()
        {
            board = initBoard.Clone() as int[,];
            pos = 0;
        }

        public void SetPosition(int p)
        {
            Reset();
            while (pos < p)
            {
                Next();
            }
        }

        void Apply(Operation op)
        {
            if (op is SelectOperation)
            {
                SelectCell = (op as SelectOperation).SelectCell;
            }
            else if (op is ChangeOperation)
            {
                var d = (op as ChangeOperation).ChangeDirection;
                Cell t = new Cell();
                switch (d)
                {
                    case Direction.Left:
                        t = SelectCell + new Cell(-1, 0);
                        break;
                    case Direction.Right:
                        t = SelectCell + new Cell(1, 0);
                        break;
                    case Direction.Up:
                        t = SelectCell + new Cell(0, -1);
                        break;
                    case Direction.Down:
                        t = SelectCell + new Cell(0, 1);
                        break;
                }
                Swap(SelectCell, t);
                SelectCell = t;
            }
        }

        void Swap(Cell a, Cell b)
        {
            var tmp = this[a];
            this[a] = this[b];
            this[b] = tmp;
        }

        public void Next()
        {
            if (pos >= seaquence.Count)
                return;
            Apply(seaquence[pos]);
            pos++;
        }

        public void Back()
        {
            SetPosition(pos-1);
        }
    }
}
