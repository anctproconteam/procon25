﻿using Procon.CommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProconSimulator
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        Simulator simulator;
        List<Button> operationButtons = new List<Button>();
        Problem problem;
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(int[,] initialBoard, OperationSequence seaquence, Problem problem)
        {
            InitializeComponent();
            simulator = new Simulator(initialBoard, seaquence.Sequence);
            this.problem = problem;
            Initialize();
            InitializeGrid();
        }

        void InitializeGrid()
        {
            var array = simulator.Board;
            var d1 = array.GetLength(0);
            var d2 = array.GetLength(1);
            MainGrid.Children.Clear();
            MainGrid.SetValue(Grid.RowProperty, d1);
            MainGrid.SetValue(Grid.ColumnProperty, d2);
            MainGrid.RowDefinitions.Clear();
            for (int i = 0; i < d1; i++)
            {
                var r = new RowDefinition();
                MainGrid.RowDefinitions.Add(r);
            }
            MainGrid.ColumnDefinitions.Clear();
            for (int i = 0; i < d2; i++)
            {
                var r = new ColumnDefinition();
                MainGrid.ColumnDefinitions.Add(r);
            }
            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    int val = array[i, j];
                    var item = new Button();
                    item.Content = val.ToString();
                    //item.Content = new Image()
                    //{
                    //    Source = problem.SplitedImage[val].ToBitmap()
                    //};
                    item.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                    item.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                    item.Background = Brushes.SkyBlue;
                    item.FontSize = 40;
                    item.FontStyle = FontStyles.Normal;

                    if (i == simulator.SelectCell.X && j == simulator.SelectCell.Y)
                    {
                        item.Background = Brushes.Yellow;
                    }

                    Grid.SetRow(item, j);
                    Grid.SetColumn(item, i);
                    MainGrid.Children.Add(item);
                }
            }
        }

        void UpdateGrid()
        {
            var array = simulator.Board;
            var d1 = array.GetLength(0);
            var d2 = array.GetLength(1);
            MainGrid.Children.Clear();
            for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2; j++)
                {
                    int val = array[i, j];
                    var item = new Button();
                    item.Content = val.ToString();
                    //item.Content = new Image()
                    //{
                    //    Source = problem.SplitedImage[val].ToBitmap()
                    //};
                    item.Background = Brushes.SkyBlue;
                    item.FontSize = 40;
                    item.FontStyle = FontStyles.Normal;
                    item.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                    item.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                    var c = j * d1 + i;
                    if (val == c)
                        item.Background = Brushes.White;
                    if (i == simulator.SelectCell.X && j == simulator.SelectCell.Y)
                    {
                        item.Background = Brushes.Yellow;
                    }

                    
                    Grid.SetRow(item, j);
                    Grid.SetColumn(item, i);
                    MainGrid.Children.Add(item);
                }
            }
            foreach (var v in operationButtons)
            {
                v.Background = Brushes.DarkGreen;
            }
            if (simulator.Pos > 0)
                operationButtons[simulator.Pos - 1].Background = Brushes.Yellow;
        }

        void LoadOperationText()
        {
            try
            {
                var seq = new List<Operation>();
                var scanner = new StringScanner(OperationText.Text);
                int w = scanner.ReadInt();
                int h = scanner.ReadInt();
                var initialBoard = new int[w, h];
                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w; x++)
                    {
                        initialBoard[x, y] = scanner.ReadInt();
                    }
                }
                var selc = scanner.ReadInt();
                for (int i = 0; i < selc; i++)
                {
                    var f = scanner.ReadChar().ToIntAs16();
                    var e = scanner.ReadChar().ToIntAs16();
                    seq.Add(new SelectOperation(new Cell(f, e)));
                    var n = scanner.ReadInt();
                    for (int j = 0; j < n; j++)
                    {
                        var d = scanner.ReadChar();
                        switch (d)
                        {
                            case 'L':
                                seq.Add(new ChangeOperation(Direction.Left));
                                break;
                            case 'R':
                                seq.Add(new ChangeOperation(Direction.Right));
                                break;
                            case 'U':
                                seq.Add(new ChangeOperation(Direction.Up));
                                break;
                            case 'D':
                                seq.Add(new ChangeOperation(Direction.Down));
                                break;
                            default:
                                break;
                        }
                    }
                }
                simulator = new Simulator(initialBoard, seq);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Title = e.ToString();
            }
        }
        void Initialize()
        {
            OperatePanel.Children.Clear();
            operationButtons.Clear();
            int i = 0;
            foreach (var v in simulator.Seaquence)
            {
                var button = new Button();
                if (v is SelectOperation)
                {
                    var s = v as SelectOperation;
                    button.Content = "Select: " + s.SelectCell.ToString();
                    DockPanel.SetDock(button, Dock.Top);
                }
                else if (v is ChangeOperation)
                {
                    var c = v as ChangeOperation;
                    button.Content = "Change: " + c.ChangeDirection.ToString();
                    DockPanel.SetDock(button, Dock.Top);
                }
                {
                    int n = ++i;
                    button.Click += (e, obj) => { simulator.SetPosition(n); UpdateGrid(); };
                }
                OperatePanel.Children.Add(button);
                operationButtons.Add(button);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LoadOperationText();
            Initialize();
            InitializeGrid();
        }

        private void Step_Button_Click(object sender, RoutedEventArgs e)
        {
            simulator.Next();
            UpdateGrid();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            simulator.Back();
            UpdateGrid();
        }
    }
}
